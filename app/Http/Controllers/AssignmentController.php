<?php

namespace App\Http\Controllers;

use App\Tblt_ReviewerAssignment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;

class AssignmentController extends Controller
{
    function index() 
    {
        return view('Assignment');
    }

    function datatablesAssignment() 
    {
        $assignment = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->leftJoin('tblt__reviewer_assignments', 'tblt__proposals.id', '=', 'tblt__reviewer_assignments.FIDProposal')
            ->leftJoin('tblm_dosen as reviewer1', 'reviewer1.NIDN', '=', 'tblt__reviewer_assignments.Reviewer1')
            ->leftJoin('tblm_dosen as reviewer2', 'reviewer2.NIDN', '=', 'tblt__reviewer_assignments.Reviewer2')
            ->select(
                    "tblt__proposals.Judul",
                    "tblt__proposals.Durasi",
                    "tblt__proposals.SatuanDurasi",
                    "tblt__proposals.RequestorID",
                    "tblt__proposals.id as FIDProposal", 
                    "tblt__reviewer_assignments.Reviewer1", 
                    "tblt__reviewer_assignments.Reviewer2",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "reviewer1.FirstName as Reviewer1FirstName",
                    "reviewer1.MiddleName as Reviewer1MiddleName",
                    "reviewer1.LastName as Reviewer1LastName",
                    "reviewer2.FirstName as Reviewer2FirstName",
                    "reviewer2.MiddleName as Reviewer2MiddleName",
                    "reviewer2.LastName as Reviewer2LastName",
                    "tblt__reviewer_assignments.CreatedBy",
                    "tblt__reviewer_assignments.ModifiedBy",
                    "tblt__reviewer_assignments.created_at",
                    "tblt__reviewer_assignments.updated_at"
                )
            ->get();
        $dataTable = Datatables::of($assignment)->make(true);

        return $dataTable;
    }

    function editAssignment($id) 
    {
        $assignment = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->join('tblt__batches', 'tblt__proposals.FIDBatch', '=', 'tblt__batches.id')
            ->leftJoin('tblt__reviewer_assignments', 'tblt__proposals.id', '=', 'tblt__reviewer_assignments.FIDProposal')
            ->leftJoin('tblm_dosen as reviewer1', 'reviewer1.NIDN', '=', 'tblt__reviewer_assignments.Reviewer1')
            ->leftJoin('tblm_dosen as reviewer2', 'reviewer2.NIDN', '=', 'tblt__reviewer_assignments.Reviewer2')
            ->select(
                    "tblt__proposals.Judul",
                    "tblt__proposals.Durasi",
                    "tblt__proposals.SatuanDurasi",
                    "tblm_dosen.NIP as RequestorID",
                    "tblt__proposals.id as FIDProposal",
                    "tblt__proposals.ProvinsiLokasi as Provinsi", 
                    "tblt__proposals.KabupatenLokasi as Kabupaten",
                    "tblt__proposals.KecamatanLokasi as Kecamatan",
                    "tblt__proposals.JarakLokasi",
                    "tblt__proposals.SatuanDurasi", 
                    "tblt__reviewer_assignments.Reviewer1", 
                    "tblt__reviewer_assignments.Reviewer2",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "reviewer1.FirstName as Reviewer1FirstName",
                    "reviewer1.MiddleName as Reviewer1MiddleName",
                    "reviewer1.LastName as Reviewer1LastName",
                    "reviewer2.FirstName as Reviewer2FirstName",
                    "reviewer2.MiddleName as Reviewer2MiddleName",
                    "reviewer2.LastName as Reviewer2LastName",
                    "tblt__reviewer_assignments.CreatedBy",
                    "tblt__reviewer_assignments.ModifiedBy",
                    "tblt__reviewer_assignments.created_at",
                    "tblt__reviewer_assignments.updated_at",
                    "tblt__batches.TahunAjaran as BatchName"
                )
            ->where('tblt__proposals.id', $id)
            ->first();

        $data= [
            'assignment'  => $assignment,
            'reviewer1'  =>  $this->dropDownReviewer($assignment->RequestorID, 'Reviewer 1'),
            'reviewer2'  =>  $this->dropDownReviewer($assignment->RequestorID, 'Reviewer 2')
        ];

        return view('EditAssignment')->with($data);
    }

    function dropDownReviewer($nip, $reviewer_type) 
    {
        $assignment = DB::table('tblm_dosen')
            ->join('tblm__users', 'tblm_dosen.NIP', '=', 'tblm__users.UserID')
            ->join('tblt_user_job_roles', 'tblt_user_job_roles.FIDUser', '=', 'tblm__users.id')
            ->join('tblm_job_roles', 'tblm_job_roles.id', '=', 'tblt_user_job_roles.FIDJobRole')
            ->select(
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "tblm_dosen.NIP"
                )
            ->where('tblm_job_roles.Description', '=', $reviewer_type)
            ->where('tblm_dosen.NIP', '!=', $nip)
            ->get();

        return $assignment;
    }

    function postEditAssignment(Request $request, $id) 
    {
        Tblt_ReviewerAssignment::updateOrCreate(
            [
                'FIDProposal' => $id
            ],
            [
                'Reviewer1'     => $request->Reviewer1,
                'Reviewer2'     => $request->Reviewer2,
                'CreatedBy'     => Session::get('userid'),
                'ModifiedBy'    => Session::get('userid')
            ]
        );

        return view('Assignment')->with(['success' => 'Success Setting Reviewer']);
    }
}

