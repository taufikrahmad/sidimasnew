<?php

namespace App\Http\Controllers;

use App\Tblm_User;
use App\Tblt_UserJobRole;
use App\Tblm_JobRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    function index() {
        return view('Login');
    }

    function loginPost(Request $request) {
        // return ('fsfdsf');
        $user_id    = $request->user_id;
        $password = trim($request->password);

        $data = Tblm_User::where('UserID',trim($user_id))->first();
        if ($data) {
            if (Hash::check($password,trim($data->Password))) {
                Session::put('userid',  $data->UserID);
                // Session::put('email', $data->email);
                Session::put('login', true);

                $isAdmin = Tblt_UserJobRole::where('FIDJobRole',7)
                                ->where('FIDUser',$data->id)
                                ->get();
                
                Session::put('isAdmin', (count($isAdmin) > 0));

                $m_roles = Tblm_JobRole::all();
                
                foreach($m_roles as $m_role){
                    //echo $m_role->Description;
                    Session::put(trim($m_role->Description), $this->hasRole($data->id,$m_role->Description));
                }


                return view('main');
            }
            else {
                return $data->Password.' salah '.$password;
            }
        }
        else {
            
            return 'user id tidak ada';
        }
    }

    function logout() {
        Session::flush();
        return redirect('');
    }

    function register(){
        return view('register');
    }

    function registerPost(Request $request) {
        // retrn('fdsfdsf');
        $this->validate($request, [
            'name'          => 'required|min:4',
            'email'         => 'required|min:4|email|unique:login',
            'password'      => 'required',
            'confirmation'  => 'required|same:password',
        ]);

        $data = new Tblm_User();
        $data->UserID = $request->user_id;
        $data->FIDPersonType = '1';
        $data->CreatedBy = 'nana';
        $data->ModifiedBy = 'nana';
        // $data->email = $request->email;
        $data->Password = Hash::make($request->password);
        
        $data->save();

        return view('Login');
    }

    private function hasRole($uid, $role){
        $user = Tblt_UserJobRole::join('tblm_job_roles', 'tblm_job_roles.id', '=', 'tblt_user_job_roles.FIDJobRole')
                                    ->where('tblm_job_roles.Description',$role)
                                    ->where('tblt_user_job_roles.FIDUser',$uid)
                                    ->get();

        return (count($user) > 0 ? 1 : 0);
    }
}
