<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DataTables;
use App\Tblt_Batch;

class BatchManagementController extends Controller
{
    function index() 
    {
        return view('BatchManagement');        
    }

    function datatablesTbltBatch() 
    {
        $dataTable = Datatables::of(Tblt_Batch::all())->make(true);

        return $dataTable;
    }

    function addBatchManagement() 
    {
        return view('AddBatchManagement');
    }

    function postAddBatchManagement(Request $request) 
    {
        $data = new Tblt_Batch();
        $data->BatchType = $request->batchType;
        $data->TahunAjaran = $request->tahunAjaran;
        $data->batch_upload_proposal = $request->batchuploadDate;
        $data->review_proposal = $request->review_proposal;
        $data->pengumuman_hasil_review_proposal = $request->pengumuman_hasil_review_proposal;
        $data->batas_upload_perbaikan = $request->batas_upload_perbaikan;
        $data->batas_laporan_hasil = $request->batas_laporan_hasil;
        $data->CreatedBy = Session::get('userid');
        $data->ModifiedBy = '';
        $data->save();

        return view('BatchManagement')->with(['success' => 'Success Add Batch']);
    }

    function editBatchManagement($id) 
    {
        $model = Tblt_Batch::findOrFail($id);

        return view('EditBatchManagement')->withModel($model);
    }

    function postEditBatchManagement(Request $request,$id) 
    {
        $data = Tblt_Batch::findOrFail($id);
        $data->BatchType = $request->batchType;
        $data->TahunAjaran = $request->tahunAjaran;
        $data->batch_upload_proposal = $request->batchuploadDate;
        $data->review_proposal = $request->review_proposal;
        $data->pengumuman_hasil_review_proposal = $request->pengumuman_hasil_review_proposal;
        $data->batas_upload_perbaikan = $request->batas_upload_perbaikan;
        $data->batas_laporan_hasil = $request->batas_laporan_hasil;
        $data->CreatedBy = $data->CreatedBy;
        $data->ModifiedBy = Session::get('userid');
        $data->save();

        return view('BatchManagement')->with(['success' => 'Success Edit Batch']);
    }

    public function deleteBatchManagement($id)
    {
        $batch = Tblt_Batch::findOrFail($id);    
        $batch->delete();

        return view('BatchManagement')->with(['success' => 'Success Delete Batch']);
    }
}
