<?php

namespace App\Http\Controllers;

use App\Tblm_JobRole;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class JobRoleController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('JobRoles');
    }

    function datatablesJobRoles() 
    {
        $dataTable = Datatables::of(Tblm_JobRole::all())->make(true);

        return $dataTable;
    }

    function addJobRoles(){
        return view('AddJobRoles');
    }

    function postAddJobRoles(Request $request){
        $data = new Tblm_JobRole();
        $data->Description = $request->description;
        $data->CreatedBy = Session::get('userid');
        $data->ModifiedBy = '';

        $data->save();

        return redirect('master-job-roles')->with(['success' => 'Success Add Role']);
    }

    function editJobRoles($id) 
    {
        $Model_JobRole = Tblm_JobRole::findOrFail($id);
        $data= [
            'ModelJobRole' => $Model_JobRole
        ];
        
        return view('EditJobRoles')->with($data);
    }

    function postEditJobRoles(Request $request, $id){
        $data = Tblm_JobRole::findOrFail($id);
        $data->Description = $request->description;
        $data->CreatedBy = $data->CreatedBy;
        $data->ModifiedBy = Session::get('userid');

        $data->save();

        return redirect('master-job-roles')->with(['success' => 'Success Edit Role']);
    }

    public function deleteJobRoles($id)
    {
        $batch = Tblm_JobRole::findOrFail($id);    
        $batch->delete();

        return redirect('master-job-roles')->with(['success' => 'Success Delete Job Role']);

    }
}
