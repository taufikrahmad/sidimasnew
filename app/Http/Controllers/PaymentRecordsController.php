<?php

namespace App\Http\Controllers;

use DataTables;
use App\Tblt_Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PaymentRecordsController extends Controller
{
    function index() 
    {
        return view('PaymentRecords');

        // return $this->datatablesAssignment();
    }

    function datatablesPaymentRecords() 
    {
        $assignment = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->leftJoin('tblt__payments', 'tblt__proposals.id', '=', 'tblt__payments.FIDProposal')
            ->select(
                    "tblt__proposals.Judul",
                    "tblt__proposals.Durasi",
                    "tblt__proposals.SatuanDurasi",
                    "tblt__proposals.RequestorID",
                    "tblt__proposals.id as FIDProposal",
                    "tblt__payments.BankName",
                    "tblt__payments.AccountNumber", 
                    "tblt__payments.TransferAmount",
                    "tblt__payments.TransferAmount2",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "tblt__payments.CreatedBy",
                    "tblt__payments.ModifiedBy",
                    "tblt__payments.created_at",
                    "tblt__payments.updated_at"
                )
            ->get();
        $dataTable = Datatables::of($assignment)->make(true);

        return $dataTable;
    }

    function editPaymentRecords($id) 
    {
        $paymentRecords = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->leftJoin('tblt__payments', 'tblt__proposals.id', '=', 'tblt__payments.FIDProposal')
            ->select(
                    "tblt__proposals.Judul",
                    "tblt__proposals.Durasi",
                    "tblt__proposals.SatuanDurasi",
                    "tblt__proposals.RequestorID",
                    "tblt__proposals.id as FIDProposal",
                    "tblt__payments.BankName",
                    "tblt__payments.AccountNumber", 
                    "tblt__payments.TransferAmount",
                    "tblt__payments.TransferAmount2",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "tblt__payments.CreatedBy",
                    "tblt__payments.ModifiedBy",
                    "tblt__payments.created_at",
                    "tblt__payments.updated_at"
                )
            ->where('tblt__proposals.id', $id)
            ->first();

        $data= [
            'paymentRecords'  => $paymentRecords
        ];

        return view('EditPaymentRecords')->with($data);
    }

    function postEditPaymentRecords(Request $request, $id) 
    {
        Tblt_Payment::updateOrCreate(
            [
                'FIDProposal' => $id
            ],
            [
                'BankName'          => $request->BankName,
                'AccountNumber'     => $request->AccountNumber,
                'TransferAmount'    => $request->TransferAmount,
                'TransferAmount2'   => $request->TransferAmount2,
                'CreatedBy'         => Session::get('userid'),
                'ModifiedBy'        => Session::get('userid')
            ]
        );

        return view('PaymentRecords')->with(['success' => 'Success Update Payment Record']);
    }
}
