<?php

namespace App\Http\Controllers;

use DataTables;
use App\Tblt_Batch;
use App\Tblt_Proposal;
use App\Tblm_TargetRencanaLuaran;
use App\Tblt_AnggotaMahasiswa;
use App\Tblt_AnggotaDosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Config;
use Illuminate\Support\Facades\DB;
use PDF;

class ProposalController extends Controller
{
    function index() 
    {
        return view('Proposal');
    }

    function datatablesProposal() 
    {
        $proposal = Tblt_Proposal::join('tblt__batches','tblt__proposals.FIDBatch','=','tblt__batches.id')
            ->leftJoin('tblt__reviewer_assignments', 'tblt__reviewer_assignments.FIDProposal', '=', 'tblt__proposals.id')
            ->leftJoin('tblt__review_results', 'tblt__review_results.FIDReviewerAssignment', '=', 'tblt__reviewer_assignments.id')
            ->where('tblt__proposals.RequestorID',Session::get('userid'))
                                ->select(
                                    
                                    "tblt__proposals.*", 
                                    "tblt__batches.BatchType", 
                                    "tblt__batches.TahunAjaran",
                                    DB::raw("(
                                        CASE 
                                        WHEN tblt__reviewer_assignments.\"id\" IS NULL THEN 'Submitted'
                                        WHEN tblt__reviewer_assignments.\"id\" IS NOT NULL AND tblt__review_results.\"id\" IS NULL THEN 'Ongoing Review' 
                                        WHEN tblt__review_results.\"FIDStatus\" = 1 AND tblt__review_results.\"FIDStatus2\" = 1 THEN 'Approved' 
                                        WHEN tblt__review_results.\"FIDStatus\" = 3 OR tblt__review_results.\"FIDStatus2\" = 3 THEN 'Rejected' 
                                        WHEN tblt__review_results.\"FIDStatus2\" = 1 THEN 'Rejected' END
                                        ) as Status ")
                                    
                                );
        $dataTable = Datatables::of($proposal)->make(true);

        return $dataTable;
    }

    function addProposal() 
    {
        $modelBatch = Tblt_Batch::get();
        $modelLuaran = Tblm_TargetRencanaLuaran::get();
        $requiredDocs = [
            [
                "desc" => "Proposal PDF",
                "id" => "proposalPDF"
            ], 
            [

                "desc" => "Proposal Doc",
                "id" => "proposalDoc"
            ],
            [

                "desc" => "Lembar Persetujuan Doc",
                "id" => "lembarPersetujuanPDF"
            ], 
            [
                "desc" => "Perbaikan Proposal PDF", 
                "id" => "PerbaikanproposalPDF"
            ],
            [

                "desc" => "Perbaikan Proposal Doc", 
                "id" => "PerbaikanproposalDoc"
            ],
            [
                "desc" => "Draft Laporan Akhir PDF",
                "id" => "DraftLaporanAkhirPDF"
            ],
            [

                "desc" => "Draft Laporan Akhir Doc", 
                "id" => "DraftLaporanAkhirDoc"
            ],
            [
                "desc" => "Laporan Akhir PDF", 
                "id" => "LaporanAkhirPDF"
            ],
            [

                "desc" => "Laporan Akhir Doc", 
                "id" => "LaporanAkhirDoc"
            ],
        ];
        
        $data= [
            'modelBatch'  => $modelBatch,
            'modelLuaran'  => $modelLuaran,
            'requiredDocs' => $requiredDocs
        ];

        return view('AddProposal')->with($data);
    }

    function postAddProposal(Request $request) 
    {
        
        $proposalPDF = '';
        $proposalDoc = '';
        $lembarPersetujuanPDF = '';
        $PerbaikanproposalPDF = '';
        $PerbaikanproposalDoc = '';
        $DraftLaporanAkhirPDF = '';
        $DraftLaporanAkhirDoc = '';
        $LaporanAkhirPDF = '';
        $LaporanAkhirDoc = '';

        // menyimpan data file yang diupload ke variabel $file
        $_fileproposalDoc = $request->file('proposalDoc');
        if($_fileproposalDoc != null){
            $proposalDoc = $_fileproposalDoc->hashName();
            $_fileproposalDoc->move(Config::get('app.pathFile'),$proposalDoc);
        }

        $_fileproposalPDF = $request->file('proposalPDF');
        if($_fileproposalPDF != null){
            $proposalPDF = $_fileproposalPDF->hashName();
            $_fileproposalPDF->move(Config::get('app.pathFile'),$proposalPDF);
        }

        
        $_filelembarPersetujuanPDF = $request->file('lembarPersetujuanPDF');
        if($_filelembarPersetujuanPDF != null){
            $lembarPersetujuanPDF = $_filelembarPersetujuanPDF->hashName();
            $_filelembarPersetujuanPDF->move(Config::get('app.pathFile'),$lembarPersetujuanPDF);
        }

        $_filePerbaikanproposalPDF = $request->file('PerbaikanproposalPDF');
        if($_filePerbaikanproposalPDF != null){
            $PerbaikanproposalPDF = $_filePerbaikanproposalPDF->hashName();
            $_filePerbaikanproposalPDF->move(Config::get('app.pathFile'),$PerbaikanproposalPDF);
        }

        $_filePerbaikanproposalDoc = $request->file('PerbaikanproposalDoc');
        if($_filePerbaikanproposalDoc != null){
            $PerbaikanproposalDoc = $_filePerbaikanproposalDoc->hashName();
            $_filePerbaikanproposalDoc->move(Config::get('app.pathFile'),$PerbaikanproposalDoc);
        }

        $_fileDraftLaporanAkhirPDF = $request->file('DraftLaporanAkhirPDF');
        if($_fileDraftLaporanAkhirPDF != null){
            $DraftLaporanAkhirPDF = $_fileDraftLaporanAkhirPDF->hashName();
            $_fileDraftLaporanAkhirPDF->move(Config::get('app.pathFile'),$DraftLaporanAkhirPDF);
        }

        $_fileDraftLaporanAkhirDoc = $request->file('DraftLaporanAkhirDoc');
        if($_fileDraftLaporanAkhirDoc != null){
            $DraftLaporanAkhirDoc = $_fileDraftLaporanAkhirDoc->hashName();
            $_fileDraftLaporanAkhirDoc->move(Config::get('app.pathFile'),$DraftLaporanAkhirDoc);
        }

        $_fileLaporanAkhirPDF = $request->file('LaporanAkhirPDF');
        if($_fileLaporanAkhirPDF != null){
            $LaporanAkhirPDF = $_fileLaporanAkhirPDF->hashName();
            $_fileLaporanAkhirPDF->move(Config::get('app.pathFile'),$LaporanAkhirPDF);
        }
        
        $_fileLaporanAkhirDoc = $request->file('LaporanAkhirDoc');
        if($_fileLaporanAkhirDoc != null){
            $LaporanAkhirDoc = $_fileLaporanAkhirDoc->hashName();
            $_fileLaporanAkhirDoc->move(Config::get('app.pathFile'),$LaporanAkhirDoc);
        }

        
        $data = new Tblt_Proposal();
        $data->FIDBatch         = $request->FIDBatch;
        $data->Judul            = $request->Judul;
        $data->ProvinsiLokasi   = $request->ProvinsiLokasi;
        $data->KabupatenLokasi  = $request->KabupatenLokasi;
        $data->KecamatanLokasi  = $request->KecamatanLokasi;
        $data->JarakLokasi      = $request->JarakLokasi;
        $data->Durasi           = $request->Durasi;
        $data->SatuanDurasi     = $request->SatuanDurasi;
        $data->RequestorID      = Session::get('userid');
        $data->CreatedBy        = Session::get('userid');
        $data->ModifiedBy       = '';
        $data->proposalPDF = $proposalPDF;
        $data->proposalDoc = $proposalDoc;
        $data->lembarPersetujuanPDF = $lembarPersetujuanPDF;
        $data->PerbaikanproposalPDF = $PerbaikanproposalPDF;
        $data->PerbaikanproposalDoc = $PerbaikanproposalDoc;
        $data->DraftLaporanAkhirPDF = $DraftLaporanAkhirPDF;
        $data->DraftLaporanAkhirDoc = $DraftLaporanAkhirDoc;
        $data->LaporanAkhirPDF = $LaporanAkhirPDF;
        $data->LaporanAkhirDoc = $LaporanAkhirDoc;
        $data->FIDLuaran = $request->targetLuaran;
        $data->budget1 = $request->budget1;
        $data->budget2 = $request->budget2;
        $data->save();

        //insert peserta mahasiswa
        $mahasiswa = explode(",",$request->selectedMahasiswa);
        foreach($mahasiswa as $value){
            $dataMahasiswa = new Tblt_AnggotaMahasiswa(); 
            $dataMahasiswa->FIDProposal = $data->id;
            $dataMahasiswa->FIDMahasiswa = $value;
            $dataMahasiswa->ModifiedBy   = '';
            $dataMahasiswa->CreatedBy        = Session::get('userid');
            $dataMahasiswa->save();
        }

        //insert peserta dosen
        $dosen = explode(",",$request->selectedDosen);
        foreach($dosen as $value){
            $dataDosen = new Tblt_AnggotaDosen(); 
            $dataDosen->FIDProposal = $data->id;
            $dataDosen->FIDDosen = $value;
            $dataDosen->save();
        }

        return view('Proposal')->with(['success' => 'Success Add New Proposal']);
        
    }

    function editProposal($id) 
    {
        $ModelProposal = Tblt_Proposal::findOrFail($id);
        $modelBatch    = Tblt_Batch::get();
        $modelLuaran = Tblm_TargetRencanaLuaran::get();
        $requiredDocs = [
            [
                "desc" => "Proposal PDF",
                "id" => "proposalPDF"
            ], 
            [

                "desc" => "Proposal Doc",
                "id" => "proposalDoc"
            ], 
            [

                "desc" => "Lembar Persetujuan Doc",
                "id" => "lembarPersetujuanPDF"
            ],
            [
                "desc" => "Perbaikan Proposal PDF", 
                "id" => "PerbaikanproposalPDF"
            ],
            [

                "desc" => "Perbaikan Proposal Doc", 
                "id" => "PerbaikanproposalDoc"
            ],
            [
                "desc" => "Draft Laporan Akhir PDF",
                "id" => "DraftLaporanAkhirPDF"
            ],
            [

                "desc" => "Draft Laporan Akhir Doc", 
                "id" => "DraftLaporanAkhirDoc"
            ],
            [
                "desc" => "Laporan Akhir PDF", 
                "id" => "LaporanAkhirPDF"
            ],
            [

                "desc" => "Laporan Akhir Doc", 
                "id" => "LaporanAkhirDoc"
            ],
        ];

        $anggotaMahasiswa = Tblt_AnggotaMahasiswa::get()
                            ->where('FIDProposal',$id);

        $anggotaDosen = Tblt_AnggotaDosen::get()
                        ->where('FIDProposal',$id);
        
        $strAnggotaMahasiswa = '';
        foreach($anggotaMahasiswa as $data){
            $strAnggotaMahasiswa .= ','.trim($data->FIDMahasiswa);
        }

        
        
        $strAnggotaDosen = '';
        foreach($anggotaDosen as $data){
            $strAnggotaDosen .= ','.trim($data->FIDDosen);
        }

        $modelBatchDate    = Tblt_Batch::where('id',$modelBatch[0]->id)
                            ->select(
                                DB::raw("
                                (
                                    CASE
                                        WHEN CURRENT_DATE BETWEEN \"batch_upload_proposal\" AND \"review_proposal\" THEN 1
                                        WHEN CURRENT_DATE BETWEEN \"pengumuman_hasil_review_proposal\" AND \"batas_laporan_hasil\" THEN 2
                                        ELSE 0
                                    END
                                ) as Status "
                                )
                            )->get();


        $data= [
            'ModelProposal' => $ModelProposal,
            'modelBatch'  => $modelBatch,
            'modelLuaran'  => $modelLuaran,
            'requiredDocs' => $requiredDocs,
            'selectedMahasiswa' => $strAnggotaMahasiswa,
            'selectedDosen' => $strAnggotaDosen,
            'isCanUpdate' => $modelBatchDate[0]->status
        ];

        return view('EditProposal')->with($data);
        
    }

    function downloadDocProposal($id) 
    {
        return response()->download(public_path()."/".Config::get('app.pathFile')."/".$id);
    }

    function postEditProposal(Request $request, $id) 
    {
        $data = Tblt_Proposal::findOrFail($id);

        $proposalPDF = '';
        $proposalDoc = '';
        $lembarPersetujuanPDF = '';
        $PerbaikanproposalPDF = '';
        $PerbaikanproposalDoc = '';
        $DraftLaporanAkhirPDF = '';
        $DraftLaporanAkhirDoc = '';
        $LaporanAkhirPDF = '';
        $LaporanAkhirDoc = '';

        // menyimpan data file yang diupload ke variabel $file
        $_fileproposalDoc = $request->file('proposalDoc');
        if($_fileproposalDoc != null){
            $proposalDoc = $_fileproposalDoc->hashName();
            $_fileproposalDoc->move(Config::get('app.pathFile'),$proposalDoc);
        }else{
           $proposalDoc = $data->proposalDoc; 
        }

        $_fileproposalPDF = $request->file('proposalPDF');
        if($_fileproposalPDF != null){
            $proposalPDF = $_fileproposalPDF->hashName();
            $_fileproposalPDF->move(Config::get('app.pathFile'),$proposalPDF);
        }else{
           $proposalPDF = $data->proposalPDF; 
        }

        $_filelembarPersetujuanPDF = $request->file('lembarPersetujuanPDF');
        if($_filelembarPersetujuanPDF != null){
            $lembarPersetujuanPDF = $_filelembarPersetujuanPDF->hashName();
            $_filelembarPersetujuanPDF->move(Config::get('app.pathFile'),$lembarPersetujuanPDF);
        }else{
            $lembarPersetujuanPDF = $data->lembarPersetujuanPDF;
        }

        $_filePerbaikanproposalPDF = $request->file('PerbaikanproposalPDF');
        if($_filePerbaikanproposalPDF != null){
            $PerbaikanproposalPDF = $_filePerbaikanproposalPDF->hashName();
            $_filePerbaikanproposalPDF->move(Config::get('app.pathFile'),$PerbaikanproposalPDF);
        }else{
            $PerbaikanproposalPDF = $data->PerbaikanproposalPDF; 
        }
        

        $_filePerbaikanproposalDoc = $request->file('PerbaikanproposalDoc');
        if($_filePerbaikanproposalDoc != null){
            $PerbaikanproposalDoc = $_filePerbaikanproposalDoc->hashName();
            $_filePerbaikanproposalDoc->move(Config::get('app.pathFile'),$PerbaikanproposalDoc);
        }else{
            $PerbaikanproposalDoc = $data->PerbaikanproposalDoc; 
        }

        $_fileDraftLaporanAkhirPDF = $request->file('DraftLaporanAkhirPDF');
        if($_fileDraftLaporanAkhirPDF != null){
            $DraftLaporanAkhirPDF = $_fileDraftLaporanAkhirPDF->hashName();
            $_fileDraftLaporanAkhirPDF->move(Config::get('app.pathFile'),$DraftLaporanAkhirPDF);
        }
        else{
            $DraftLaporanAkhirPDF = $data->DraftLaporanAkhirPDF; 
        }

        $_fileDraftLaporanAkhirDoc = $request->file('DraftLaporanAkhirDoc');
        if($_fileDraftLaporanAkhirDoc != null){
            $DraftLaporanAkhirDoc = $_fileDraftLaporanAkhirDoc->hashName();
            $_fileDraftLaporanAkhirDoc->move(Config::get('app.pathFile'),$DraftLaporanAkhirDoc);
        }else{
            $DraftLaporanAkhirDoc = $data->DraftLaporanAkhirDoc; 
        }

        $_fileLaporanAkhirPDF = $request->file('LaporanAkhirPDF');
        if($_fileLaporanAkhirPDF != null){
            $LaporanAkhirPDF = $_fileLaporanAkhirPDF->hashName();
            $_fileLaporanAkhirPDF->move(Config::get('app.pathFile'),$LaporanAkhirPDF);
        }else{
            $LaporanAkhirPDF = $data->LaporanAkhirPDF; 
        }
        
        $_fileLaporanAkhirDoc = $request->file('LaporanAkhirDoc');
        if($_fileLaporanAkhirDoc != null){
            $LaporanAkhirDoc = $_fileLaporanAkhirDoc->hashName();
            $_fileLaporanAkhirDoc->move(Config::get('app.pathFile'),$LaporanAkhirDoc);
        }else{
            $LaporanAkhirDoc = $data->LaporanAkhirDoc; 
        }

        $data->FIDBatch         = $request->FIDBatch;
        $data->Judul            = $request->Judul;
        $data->ProvinsiLokasi   = $request->ProvinsiLokasi;
        $data->KabupatenLokasi  = $request->KabupatenLokasi;
        $data->KecamatanLokasi  = $request->KecamatanLokasi;
        $data->JarakLokasi      = $request->JarakLokasi;
        $data->Durasi           = $request->Durasi;
        $data->SatuanDurasi     = $request->SatuanDurasi;
        $data->RequestorID      = $data->RequestorID;
        $data->CreatedBy        = $data->CreatedBy;
        $data->ModifiedBy       = Session::get('userid');
        $data->proposalPDF = $proposalPDF;
        $data->proposalDoc = $proposalDoc;
        $data->lembarPersetujuanPDF = $lembarPersetujuanPDF;
        $data->PerbaikanproposalPDF = $PerbaikanproposalPDF;
        $data->PerbaikanproposalDoc = $PerbaikanproposalDoc;
        $data->DraftLaporanAkhirPDF = $DraftLaporanAkhirPDF;
        $data->DraftLaporanAkhirDoc = $DraftLaporanAkhirDoc;
        $data->LaporanAkhirPDF = $LaporanAkhirPDF;
        $data->LaporanAkhirDoc = $LaporanAkhirDoc;
        $data->FIDLuaran = $request->targetLuaran;
        $data->budget1 = $request->budget1;
        $data->budget2 = $request->budget2;
        
        
        
        $data->save();

        DB::table('tblt__anggota_dosens')->where('FIDProposal', $id)->delete();
        DB::table('tblt__anggota_mahasiswas')->where('FIDProposal', $id)->delete();

        //insert peserta mahasiswa
        $mahasiswa = explode(",",$request->selectedMahasiswa);
        foreach($mahasiswa as $value){
            $dataMahasiswa = new Tblt_AnggotaMahasiswa(); 
            $dataMahasiswa->FIDProposal = $data->id;
            $dataMahasiswa->FIDMahasiswa = $value;
            $dataMahasiswa->ModifiedBy   = '';
            $dataMahasiswa->CreatedBy        = Session::get('userid');
            $dataMahasiswa->save();
        }

        //insert peserta dosen
        $dosen = explode(",",$request->selectedDosen);
        foreach($dosen as $value){
            $dataDosen = new Tblt_AnggotaDosen(); 
            $dataDosen->FIDProposal = $data->id;
            $dataDosen->FIDDosen = $value;
            $dataDosen->save();
        }

        return view('Proposal')->with(['success' => 'Success Add Role']);
    }

    function AnggotaMahasiswa($id){
        $term = trim($id);


        if (empty($term)) {
            return \Response::json([]);
        }

        $tblm_Mahasiswas = DB::select( DB::raw('
            SELECT "NIM","Name" 
            FROM tblm_mahasiswas 
            INNER JOIN  tblt__anggota_mahasiswas ON trim(tblm_mahasiswas."NIM") = trim(tblt__anggota_mahasiswas."FIDMahasiswa")
            WHERE 
                tblt__anggota_mahasiswas."FIDProposal" = '.$term
            )
        );


        $formatted_tags = [];

        foreach ($tblm_Mahasiswas as $tblm_Mahasiswa) {
            $formatted_tags[] = ['id' => trim($tblm_Mahasiswa->NIM), 'text' => '['.trim($tblm_Mahasiswa->NIM).'] - '.trim($tblm_Mahasiswa->Name)];
        }

        return \Response::json($formatted_tags);
    }


    function AnggotaDosen($id){
        $term = trim($id);


        if (empty($term)) {
            return \Response::json([]);
        }

        $tblm_Mahasiswas = DB::select( DB::raw('
            SELECT 
                UPPER(trim("NIP")) as NIP,
                trim("FirstName") as FirstName, 
                trim("MiddleName") as MiddleName,
                trim("LastName") as LastName
            FROM tblm_dosen 
            INNER JOIN  tblt__anggota_dosens 
                ON trim(tblm_dosen."NIP") = trim(tblt__anggota_dosens."FIDDosen")
            WHERE 
                tblt__anggota_dosens."FIDProposal" = '.$term
            )
        );


        $formatted_tags = [];

        foreach ($tblm_Mahasiswas as $tblm_Dosen) {
            $formatted_tags[] = ['id' => trim($tblm_Dosen->nip), 
                'text' => '['.trim($tblm_Dosen->nip).'] - '
                    .trim($tblm_Dosen->firstname).' '
                    .trim($tblm_Dosen->middlename).' '
                    .trim($tblm_Dosen->lastname)
            ];
        }

        return \Response::json($formatted_tags);
    }

    public function cetakPersetujuan($id)
    {
        $proposal = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->join('tblm_fakultas', 'tblm_dosen.FIDFakultas', '=', 'tblm_fakultas.id')
            ->join('tblm_jfas', 'tblm_dosen.FIDJfa', '=', 'tblm_jfas.id')
            ->join('tblt__batches', 'tblt__proposals.FIDBatch', '=', 'tblt__batches.id')
            ->join('tblm_target_rencana_luaran', 'tblt__proposals.FIDLuaran', '=', 'tblm_target_rencana_luaran.id')
            ->leftJoin('tblm_education_levels', 'tblm_dosen.FIDEducation', '=', 'tblm_education_levels.id')
            ->leftJoin('tblm_grade_dosens', 'tblm_dosen.FIDGrade', '=', 'tblm_grade_dosens.id')
            ->leftJoin('tblt__reviewer_assignments', 'tblt__reviewer_assignments.FIDProposal', '=', 'tblt__proposals.id')
            ->leftJoin('tblt__review_results', 'tblt__review_results.FIDReviewerAssignment', '=', 'tblt__reviewer_assignments.id')
            ->where('tblt__proposals.id',$id)
            ->select(
                    "tblt__proposals.*",
                    "tblm_fakultas.Description as NamaFakultas",
                    "tblm_jfas.Description as NamaJfa",
                    "tblm_education_levels.Description as NamaPendidikan",
                    "tblm_grade_dosens.Description as Grade",
                    "tblt__batches.BatchType",
                    "tblt__batches.TahunAjaran",
                    "tblm_target_rencana_luaran.Description as RencanaLuaran",
                    "tblm_dosen.NIDN",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "tblm_dosen.Phone",
                    "tblm_dosen.Email"
                )
            ->get();

        

        $anggotaMahasiswa = DB::table('tblt__anggota_mahasiswas')
                            ->join('tblm_mahasiswas', 'tblm_mahasiswas.NIM', '=', 'tblt__anggota_mahasiswas.FIDMahasiswa')
                            ->where('FIDProposal',$id)
                            ->select('tblm_mahasiswas.*')
                            ->get();

        $anggotaDosen = DB::table('tblt__anggota_dosens')
                            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__anggota_dosens.FIDDosen')
                            ->where('FIDProposal',$id)
                            ->select('tblm_dosen.*')
                            ->get();
        

        $data= [
            'dataProposal' => $proposal,
            'dataAnggotaMahasiswa'  => $anggotaMahasiswa,
            'dataAnggotaDosen'  => $anggotaDosen
        ];


        //return $proposal;
    	$pdf = PDF::loadview('_templateLembarPersetujuan',$data);
    	return $pdf->download('lembar-pengesahan-pdf.pdf');
    }

    public function downloadSKProposal($id){
        $proposal = DB::table('tblt__proposals')
        ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
        ->join('tblm_fakultas', 'tblm_dosen.FIDFakultas', '=', 'tblm_fakultas.id')
        ->join('tblm_jfas', 'tblm_dosen.FIDJfa', '=', 'tblm_jfas.id')
        ->join('tblt__batches', 'tblt__proposals.FIDBatch', '=', 'tblt__batches.id')
        ->join('tblm_target_rencana_luaran', 'tblt__proposals.FIDLuaran', '=', 'tblm_target_rencana_luaran.id')
        ->leftJoin('tblm_education_levels', 'tblm_dosen.FIDEducation', '=', 'tblm_education_levels.id')
        ->leftJoin('tblm_grade_dosens', 'tblm_dosen.FIDGrade', '=', 'tblm_grade_dosens.id')
        ->leftJoin('tblt__reviewer_assignments', 'tblt__reviewer_assignments.FIDProposal', '=', 'tblt__proposals.id')
        ->leftJoin('tblt__review_results', 'tblt__review_results.FIDReviewerAssignment', '=', 'tblt__reviewer_assignments.id')
        ->where('tblt__proposals.id',$id)
        ->select(
                "tblt__proposals.*",
                "tblm_fakultas.Description as NamaFakultas",
                "tblm_jfas.Description as NamaJfa",
                "tblm_education_levels.Description as NamaPendidikan",
                "tblm_grade_dosens.Description as Grade",
                "tblt__batches.BatchType",
                "tblt__batches.TahunAjaran",
                "tblm_target_rencana_luaran.Description as RencanaLuaran",
                "tblm_dosen.NIDN",
                "tblm_dosen.FirstName",
                "tblm_dosen.MiddleName",
                "tblm_dosen.LastName",
                "tblm_dosen.Phone",
                "tblm_dosen.Email"
            )
        ->get();

    

        $anggotaMahasiswa = DB::table('tblt__anggota_mahasiswas')
                            ->join('tblm_mahasiswas', 'tblm_mahasiswas.NIM', '=', 'tblt__anggota_mahasiswas.FIDMahasiswa')
                            ->where('FIDProposal',$id)
                            ->select('tblm_mahasiswas.*')
                            ->get();

        $anggotaDosen = DB::table('tblt__anggota_dosens')
                            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__anggota_dosens.FIDDosen')
                            ->where('FIDProposal',$id)
                            ->select('tblm_dosen.*')
                            ->get();
        

        $data= [
            'dataProposal' => $proposal,
            'dataAnggotaMahasiswa'  => $anggotaMahasiswa,
            'dataAnggotaDosen'  => $anggotaDosen
        ];


        //return $proposal;
        $pdf = PDF::loadview('_templateSKProposal',$data);
        return $pdf->download('lembar-sk-proposal.pdf');
    }
}
