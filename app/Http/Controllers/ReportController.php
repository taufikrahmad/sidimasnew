<?php

namespace App\Http\Controllers;

use PDF;
use App\Tblt_Batch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function cetakReportBatch()
    {
        $batch = Tblt_Batch::all();  

        $data= [
            'batch' => $batch
        ];
        $pdf = PDF::loadview('_templateReportBatch',$data)->setPaper('a4', 'landscape');
        
    	return $pdf->download('report-batch.pdf');
    }

    public function ReportProposalBatch()
    {    
        $batch = Tblt_Batch::all();  

        $data= [
            'batch' => $batch
        ];

    	return view('ProposalBatch')->with($data);
    }

    public function cetakReportProposalBatch(Request $request)
    {        
        $FIDBatch = $request->FIDBatch;

    	$batch = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->join('tblt__batches', 'tblt__proposals.FIDBatch', '=', 'tblt__batches.id')
            ->leftJoin('tblt__reviewer_assignments', 'tblt__proposals.id', '=', 'tblt__reviewer_assignments.FIDProposal')
            ->leftJoin('tblm_dosen as reviewer1', 'reviewer1.NIDN', '=', 'tblt__reviewer_assignments.Reviewer1')
            ->leftJoin('tblm_dosen as reviewer2', 'reviewer2.NIDN', '=', 'tblt__reviewer_assignments.Reviewer2')
            ->select(
                    "tblt__proposals.Judul",
                    "tblt__proposals.Durasi",
                    "tblt__proposals.SatuanDurasi",
                    "tblt__proposals.budget1",
                    "tblt__proposals.budget2",
                    "tblm_dosen.NIP as RequestorID",
                    "tblt__proposals.id as FIDProposal",
                    "tblt__proposals.ProvinsiLokasi as Provinsi", 
                    "tblt__proposals.KabupatenLokasi as Kabupaten",
                    "tblt__proposals.KecamatanLokasi as Kecamatan",
                    "tblt__proposals.JarakLokasi", 
                    "tblt__reviewer_assignments.Reviewer1", 
                    "tblt__reviewer_assignments.Reviewer2",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "reviewer1.FirstName as Reviewer1FirstName",
                    "reviewer1.MiddleName as Reviewer1MiddleName",
                    "reviewer1.LastName as Reviewer1LastName",
                    "reviewer2.FirstName as Reviewer2FirstName",
                    "reviewer2.MiddleName as Reviewer2MiddleName",
                    "reviewer2.LastName as Reviewer2LastName",
                    "tblt__reviewer_assignments.CreatedBy",
                    "tblt__reviewer_assignments.ModifiedBy",
                    "tblt__reviewer_assignments.created_at",
                    "tblt__reviewer_assignments.updated_at",
                    "tblt__batches.TahunAjaran as BatchName"
                )
            ->where('tblt__proposals.FIDBatch', $FIDBatch)
            ->get();

        $data= [
            'batch' => $batch
        ];

        $pdf = PDF::loadview('_templateReportProposalBatch',$data)->setPaper('a4', 'landscape');
    
        return $pdf->download('report-proposal-batch.pdf');
    }

    public function ReportPaymentBatch()
    {    
        $batch = Tblt_Batch::all();  

        $data= [
            'batch' => $batch
        ];

    	return view('PaymentBatch')->with($data);
    }

    function cetakReportPaymentBatch(Request $request) 
    {
        $FIDBatch = $request->FIDBatch;

        $payment = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->join('tblt__batches', 'tblt__proposals.FIDBatch', '=', 'tblt__batches.id')
            ->leftJoin('tblt__reviewer_assignments', 'tblt__proposals.id', '=', 'tblt__reviewer_assignments.FIDProposal')
            ->leftJoin('tblm_dosen as reviewer1', 'reviewer1.NIDN', '=', 'tblt__reviewer_assignments.Reviewer1')
            ->leftJoin('tblm_dosen as reviewer2', 'reviewer2.NIDN', '=', 'tblt__reviewer_assignments.Reviewer2')
            ->leftJoin('tblt__payments', 'tblt__proposals.id', '=', 'tblt__payments.FIDProposal')
            ->select(
                    "tblt__proposals.Judul",
                    "tblt__proposals.Durasi",
                    "tblt__proposals.SatuanDurasi",
                    "tblt__proposals.budget1",
                    "tblt__proposals.budget2",
                    "tblm_dosen.NIP as RequestorID",
                    "tblt__proposals.id as FIDProposal",
                    "tblt__proposals.ProvinsiLokasi as Provinsi", 
                    "tblt__proposals.KabupatenLokasi as Kabupaten",
                    "tblt__proposals.KecamatanLokasi as Kecamatan",
                    "tblt__proposals.JarakLokasi", 
                    "tblt__reviewer_assignments.Reviewer1", 
                    "tblt__reviewer_assignments.Reviewer2",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "reviewer1.FirstName as Reviewer1FirstName",
                    "reviewer1.MiddleName as Reviewer1MiddleName",
                    "reviewer1.LastName as Reviewer1LastName",
                    "reviewer2.FirstName as Reviewer2FirstName",
                    "reviewer2.MiddleName as Reviewer2MiddleName",
                    "reviewer2.LastName as Reviewer2LastName",
                    "tblt__reviewer_assignments.CreatedBy",
                    "tblt__reviewer_assignments.ModifiedBy",
                    "tblt__reviewer_assignments.created_at",
                    "tblt__reviewer_assignments.updated_at",
                    "tblt__batches.TahunAjaran as BatchName",
                    "tblt__payments.BankName",
                    "tblt__payments.AccountNumber", 
                    "tblt__payments.TransferAmount",
                    "tblt__payments.TransferAmount2"
                )
            ->where('tblt__proposals.FIDBatch', $FIDBatch)
            ->get();

        $data= [
            'payment'  => $payment
        ];

        $pdf = PDF::loadview('_templateReportPayment',$data)->setPaper('a4', 'landscape');
    
        return $pdf->download('report-payment-batch.pdf');
    }
}
