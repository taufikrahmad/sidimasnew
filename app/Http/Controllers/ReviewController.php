<?php

namespace App\Http\Controllers;

use App\Tblt_ReviewerAssignment;
use App\Tblt_Proposal;
use App\Tblt_ReviewResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('Review');
    }

    function datatablesAssignment() 
    {
        $assignment = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->leftJoin('tblt__reviewer_assignments', 'tblt__proposals.id', '=', 'tblt__reviewer_assignments.FIDProposal')
            ->leftJoin('tblm_dosen as reviewer1', 'reviewer1.NIDN', '=', 'tblt__reviewer_assignments.Reviewer1')
            ->leftJoin('tblm_dosen as reviewer2', 'reviewer2.NIDN', '=', 'tblt__reviewer_assignments.Reviewer2')
            ->where('tblt__reviewer_assignments.Reviewer1',Session::get('userid'))
            ->orWhere('tblt__reviewer_assignments.Reviewer2',Session::get('userid'))
            ->select(
                    "tblt__proposals.Judul",
                    "tblt__proposals.Durasi",
                    "tblt__proposals.SatuanDurasi",
                    "tblt__proposals.RequestorID",
                    "tblt__proposals.id as FIDProposal", 
                    "tblt__reviewer_assignments.Reviewer1", 
                    "tblt__reviewer_assignments.Reviewer2",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "reviewer1.FirstName as Reviewer1FirstName",
                    "reviewer1.MiddleName as Reviewer1MiddleName",
                    "reviewer1.LastName as Reviewer1LastName",
                    "reviewer2.FirstName as Reviewer2FirstName",
                    "reviewer2.MiddleName as Reviewer2MiddleName",
                    "reviewer2.LastName as Reviewer2LastName",
                    "tblt__reviewer_assignments.CreatedBy",
                    "tblt__reviewer_assignments.ModifiedBy",
                    "tblt__reviewer_assignments.created_at",
                    "tblt__reviewer_assignments.updated_at"
                )
            ->get();
        $dataTable = Datatables::of($assignment)->make(true);

        return $dataTable;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = '';

        //
        $review1 = DB::table('tblt__reviewer_assignments')
            ->where('FIDProposal',$id)
            ->where('Reviewer1',Session::get('userid'))
            ->select(
                    "Reviewer1"
                )
            ->get();

        $review2 = DB::table('tblt__reviewer_assignments')
            ->join('tblt__review_results', 'tblt__review_results.FIDReviewerAssignment', '=', 'tblt__reviewer_assignments.id')
            ->where('tblt__reviewer_assignments.FIDProposal',$id)
            ->where('tblt__reviewer_assignments.Reviewer2',Session::get('userid'))
            ->where('tblt__review_results.FIDStatus',1)
            ->select(
                    "tblt__reviewer_assignments.Reviewer2"
                )
            ->get();

        if(count($review1)){
            $job = (trim($review1[0]->Reviewer1) != '' ? 'Reviewer 1' : '');
        }elseif (count($review2)) {
            # code...
            $job = (trim($review2[0]->Reviewer2) != '' ? 'Reviewer 2' : '');
        }

        if($job == ''){
            return abort(403,'Anda tidak memiliki akses ke proposal ini');
        }

        $proposal = DB::table('tblt__proposals')
            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__proposals.RequestorID')
            ->join('tblm_fakultas', 'tblm_dosen.FIDFakultas', '=', 'tblm_fakultas.id')
            ->join('tblm_jfas', 'tblm_dosen.FIDJfa', '=', 'tblm_jfas.id')
            ->join('tblt__batches', 'tblt__proposals.FIDBatch', '=', 'tblt__batches.id')
            ->join('tblm_target_rencana_luaran', 'tblt__proposals.FIDLuaran', '=', 'tblm_target_rencana_luaran.id')
            ->leftJoin('tblm_education_levels', 'tblm_dosen.FIDEducation', '=', 'tblm_education_levels.id')
            ->leftJoin('tblm_grade_dosens', 'tblm_dosen.FIDGrade', '=', 'tblm_grade_dosens.id')
            ->where('tblt__proposals.id',$id)
            ->select(
                    "tblt__proposals.*",
                    "tblm_fakultas.Description as NamaFakultas",
                    "tblm_jfas.Description as NamaJfa",
                    "tblm_education_levels.Description as NamaPendidikan",
                    "tblm_grade_dosens.Description as Grade",
                    "tblt__batches.BatchType",
                    "tblt__batches.TahunAjaran",
                    "tblm_target_rencana_luaran.Description as RencanaLuaran",
                    "tblm_dosen.NIDN",
                    "tblm_dosen.FirstName",
                    "tblm_dosen.MiddleName",
                    "tblm_dosen.LastName",
                    "tblm_dosen.Phone",
                    "tblm_dosen.Email"
                )
            ->get();

        

        $anggotaMahasiswa = DB::table('tblt__anggota_mahasiswas')
                            ->join('tblm_mahasiswas', 'tblm_mahasiswas.NIM', '=', 'tblt__anggota_mahasiswas.FIDMahasiswa')
                            ->where('FIDProposal',$id)
                            ->select('tblm_mahasiswas.*')
                            ->get();

        $anggotaDosen = DB::table('tblt__anggota_dosens')
                            ->join('tblm_dosen', 'tblm_dosen.NIDN', '=', 'tblt__anggota_dosens.FIDDosen')
                            ->where('FIDProposal',$id)
                            ->select('tblm_dosen.*')
                            ->get();

                            $requiredDocs = [
                                [
                                    "desc" => "Proposal PDF",
                                    "id" => "proposalPDF"
                                ], 
                                [
                    
                                    "desc" => "Proposal Doc",
                                    "id" => "proposalDoc"
                                ], 
                                [
                                    "desc" => "Perbaikan Proposal PDF", 
                                    "id" => "PerbaikanproposalPDF"
                                ],
                                [
                    
                                    "desc" => "Perbaikan Proposal Doc", 
                                    "id" => "PerbaikanproposalDoc"
                                ],
                                [
                                    "desc" => "Draft Laporan Akhir PDF",
                                    "id" => "DraftLaporanAkhirPDF"
                                ],
                                [
                    
                                    "desc" => "Draft Laporan Akhir Doc", 
                                    "id" => "DraftLaporanAkhirDoc"
                                ],
                                [
                                    "desc" => "Laporan Akhir PDF", 
                                    "id" => "LaporanAkhirPDF"
                                ],
                                [
                    
                                    "desc" => "Laporan Akhir Doc", 
                                    "id" => "LaporanAkhirDoc"
                                ],
                            ];

        $reviewerAssignment = DB::table('tblt__reviewer_assignments')
                              ->where('FIDProposal',$id)
                              ->get();

        $reviewResult = DB::table('tblt__review_results')
                        ->where('FIDReviewerAssignment',$reviewerAssignment[0]->id)
                        ->get();

        $data= [
            'dataProposal' => $proposal,
            'dataAnggotaMahasiswa'  => $anggotaMahasiswa,
            'dataAnggotaDosen'  => $anggotaDosen,
            'dataJob' => $job,
            'requiredDocs' => $requiredDocs,
            'dataAssignment' => $reviewerAssignment,
            'dataReviewResult' => $reviewResult,
        ];

        return view('EditReview')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $data = new Tblt_ReviewResult();
        if($request->resultID != 0){
            $data = Tblt_ReviewResult::findOrFail($request->resultID);
            $data->ModifiedBy = Session::get('userid');
        }else{
            $data->FIDReviewerAssignment = $id;
            $data->CreatedBy = Session::get('userid');
            $data->ModifiedBy = '';
        }

        if($request->role == 'Reviewer 1'){
            $data->Remarks = $request->remarks;
            $data->FIDStatus = $request->status;
        }else{
            $data->Remarks2 = $request->remarks;
            $data->FIDStatus2 = $request->status;
        }

        $data->save();

        
        return redirect('review')->with(['success' => 'Success Review Proposal']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
