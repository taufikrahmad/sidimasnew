<?php

namespace App\Http\Controllers;

use App\Tblm_Dosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TblmDosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tblm_Dosen  $tblm_Dosen
     * @return \Illuminate\Http\Response
     */
    public function show(Tblm_Dosen $tblm_Dosen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tblm_Dosen  $tblm_Dosen
     * @return \Illuminate\Http\Response
     */
    public function edit(Tblm_Dosen $tblm_Dosen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tblm_Dosen  $tblm_Dosen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tblm_Dosen $tblm_Dosen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tblm_Dosen  $tblm_Dosen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tblm_Dosen $tblm_Dosen)
    {
        //
    }

    public function find(Request $request)
    {
        $term = trim($request->q);


        if (empty($term)) {
            return \Response::json([]);
        }

        $tblm_Dosens = DB::select( DB::raw('SELECT 
                                            UPPER(trim("NIP")) as NIP,
                                            trim("FirstName") as FirstName, 
                                            trim("MiddleName") as MiddleName,
                                            trim("LastName") as LastName
                                        FROM tblm_dosen 
                                        WHERE 
                                            UPPER(trim("FirstName")) LIKE \'%'.strtoupper($term).'%\' 
                                                OR
                                            UPPER(trim("MiddleName")) LIKE \'%'.strtoupper($term).'%\' 
                                                OR
                                            UPPER(trim("LastName")) LIKE \'%'.strtoupper($term).'%\' 
                                                OR 
                                            UPPER(trim("NIP")) LIKE \'%'.strtoupper($term).'%\' 
                                        ') );


        $formatted_tags = [];

        foreach ($tblm_Dosens as $tblm_Dosen) {
            $formatted_tags[] = ['id' => trim($tblm_Dosen->nip), 
                'text' => '['.trim($tblm_Dosen->nip).'] - '
                    .trim($tblm_Dosen->firstname).' '
                    .trim($tblm_Dosen->middlename).' '
                    .trim($tblm_Dosen->lastname)
            ];
        }

        return \Response::json($formatted_tags);
    }
}
