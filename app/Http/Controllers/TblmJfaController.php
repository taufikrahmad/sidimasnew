<?php

namespace App\Http\Controllers;
use App\Tblm_Jfa;
use DataTables;
use App\Tblm_TargetRencanaLuaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TblmJfaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('Jfa');
    }

    function datatablesJfa() 
    {
        $dataTable = Datatables::of(Tblm_Jfa::all())->make(true);

        return $dataTable;
    }

    function addJfa(){
        return view('AddJfa');
    }

    function postAddJfa (Request $request){
        $data = new Tblm_Jfa();
        $data->Description = $request->description;
        $data->CreatedBy = Session::get('userid');
        $data->ModifiedBy = '';

        $data->save();

        return redirect('master-jfa');
    }

    function editJfa($id) 
    {
        $Model_Jfa = Tblm_Jfa::findOrFail($id);
        $data= [
            'ModelJfa' => $Model_Jfa
        ];
        
        return view('EditJfa')->with($data);
    }

    function postEditJfa(Request $request, $id){
        $data = Tblm_Jfa::findOrFail($id);
        $data->Description = $request->description;
        $data->CreatedBy = $data->CreatedBy;
        $data->ModifiedBy = Session::get('userid');

        $data->save();

        return redirect('master-jfa');
    }

    public function deleteJfa($id)
    {
        $batch = Tblm_Jfa::findOrFail($id);    
        $batch->delete();

        return redirect('master-jfa');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tblm_Jfa  $tblm_Jfa
     * @return \Illuminate\Http\Response
     */
    public function show(Tblm_Jfa $tblm_Jfa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tblm_Jfa  $tblm_Jfa
     * @return \Illuminate\Http\Response
     */
    public function edit(Tblm_Jfa $tblm_Jfa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tblm_Jfa  $tblm_Jfa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tblm_Jfa $tblm_Jfa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tblm_Jfa  $tblm_Jfa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tblm_Jfa $tblm_Jfa)
    {
        //
    }
}
