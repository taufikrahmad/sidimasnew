<?php

namespace App\Http\Controllers;

use App\Tblm_Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TblmMahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tblm_Mahasiswa  $tblm_Mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function show(Tblm_Mahasiswa $tblm_Mahasiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tblm_Mahasiswa  $tblm_Mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Tblm_Mahasiswa $tblm_Mahasiswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tblm_Mahasiswa  $tblm_Mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tblm_Mahasiswa $tblm_Mahasiswa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tblm_Mahasiswa  $tblm_Mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tblm_Mahasiswa $tblm_Mahasiswa)
    {
        //
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request)
    {
        $term = trim($request->q);


        if (empty($term)) {
            return \Response::json([]);
        }

        $tblm_Mahasiswas = DB::select( DB::raw('SELECT "NIM","Name" FROM tblm_mahasiswas WHERE UPPER(trim("Name")) LIKE \'%'.strtoupper($term).'%\' OR UPPER(trim("NIM")) LIKE \'%'.strtoupper($term).'%\' ') );


        $formatted_tags = [];

        foreach ($tblm_Mahasiswas as $tblm_Mahasiswa) {
            $formatted_tags[] = ['id' => trim($tblm_Mahasiswa->NIM), 'text' => '['.trim($tblm_Mahasiswa->NIM).'] - '.trim($tblm_Mahasiswa->Name)];
        }

        return \Response::json($formatted_tags);
    }
}
