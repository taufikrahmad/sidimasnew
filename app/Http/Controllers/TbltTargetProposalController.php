<?php

namespace App\Http\Controllers;

use App\tblt_target_proposal;
use DataTables;
use App\Tblm_TargetRencanaLuaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TbltTargetProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('MTarget');
    }

    function datatablesMTarget() 
    {
        $dataTable = Datatables::of(Tblm_TargetRencanaLuaran::all())->make(true);

        return $dataTable;
    }

    function addTarget(){
        return view('AddMTarget');
    }

    function postAddTarget(Request $request){
        $data = new Tblm_TargetRencanaLuaran();
        $data->Description = $request->description;
        $data->CreatedBy = Session::get('userid');
        $data->ModifiedBy = '';

        $data->save();

        return view('MTarget');
    }

    function editTarget($id) 
    {
        $Model_TargetRencanaLuaran = Tblm_TargetRencanaLuaran::findOrFail($id);
        $data= [
            'ModelMTarget' => $Model_TargetRencanaLuaran
        ];
        
        return view('EditMTarget')->with($data);
    }

    function postEditTarget(Request $request, $id){
        $data = Tblm_TargetRencanaLuaran::findOrFail($id);
        $data->Description = $request->description;
        $data->CreatedBy = $data->CreatedBy;
        $data->ModifiedBy = Session::get('userid');

        $data->save();

        return view('MTarget');
    }

    public function deleteTarget($id)
    {
        $batch = Tblm_TargetRencanaLuaran::findOrFail($id);    
        $batch->delete();

        return view('MTarget');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tblt_target_proposal  $tblt_target_proposal
     * @return \Illuminate\Http\Response
     */
    public function show(tblt_target_proposal $tblt_target_proposal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tblt_target_proposal  $tblt_target_proposal
     * @return \Illuminate\Http\Response
     */
    public function edit(tblt_target_proposal $tblt_target_proposal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tblt_target_proposal  $tblt_target_proposal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tblt_target_proposal $tblt_target_proposal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tblt_target_proposal  $tblt_target_proposal
     * @return \Illuminate\Http\Response
     */
    public function destroy(tblt_target_proposal $tblt_target_proposal)
    {
        //
    }
}
