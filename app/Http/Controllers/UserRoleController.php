<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tblm_Dosen;
use App\Tblm_JobRole;
use App\Tblt_UserJobRole;
use Illuminate\Support\Facades\Session;
use DataTables;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('UserRole');
    }


    function datatablesDosen() 
    {
        $assignment = Tblm_Dosen::join('tblm_fakultas','tblm_fakultas.id', '=', 'tblm_dosen.FIDFakultas')
            ->join('tblm__users','tblm__users.UserID', '=', 'tblm_dosen.NIDN')
            ->select(
                    "tblm_dosen.*",
                    "tblm__users.id as idUser",
                    "tblm_fakultas.Description as FacultyDesc"
                )
            ->get();
        $dataTable = Datatables::of($assignment)->make(true);

        return $dataTable;
    }

    function datatablesDosenRole($id){
        $assignment = Tblm_Dosen::join('tblm_fakultas','tblm_fakultas.id', '=', 'tblm_dosen.FIDFakultas')
            ->join('tblm__users','tblm__users.UserID', '=', 'tblm_dosen.NIDN')
            ->join('tblt_user_job_roles','tblt_user_job_roles.FIDUser', '=', 'tblm__users.id')
            ->join('tblm_job_roles','tblm_job_roles.id', '=', 'tblt_user_job_roles.FIDJobRole')
            ->where('tblm__users.id',$id)
            ->select(
                    "tblm_dosen.*",
                    "tblm__users.id as idUser",
                    "tblt_user_job_roles.id as idUserRole",
                    "tblm_fakultas.Description as FacultyDesc",
                    "tblm_job_roles.Description as RoleDesc"
                )
            ->get();
        $dataTable = Datatables::of($assignment)->make(true);

        return $dataTable;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $dosen = Tblm_Dosen::join('tblm_fakultas','tblm_fakultas.id', '=', 'tblm_dosen.FIDFakultas')
            ->join('tblm__users','tblm__users.UserID', '=', 'tblm_dosen.NIDN')
            ->where('tblm__users.id',$id)
            ->select(
                    "tblm_dosen.*",
                    "tblm__users.id as idUser",
                    "tblm_fakultas.Description as FacultyDesc"
                )
            ->get();

        $roles = Tblm_JobRole::all();
        
        $data = [
            'dataDosen' => $dosen,
            'dataMRoles' => $roles
        ];

        return view('edit-UserRole')->with($data);
    }

    public function postedit(Request $request,$id){
        $data = new Tblt_UserJobRole();
        $data->FIDUser = $id;
        $data->FIDJobRole = $request->role;
        $data->CreatedBy        = Session::get('userid');
        $data->ModifiedBy       = '';
        $data->save();

        return redirect('master-user-roles')->with(['success' => 'Success Add Role to User']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $userRole = Tblt_UserJobRole::findOrFail($id);    
        $userRole->delete();

        return redirect('master-user-roles')->with(['success' => 'Success Delete Role from User']);
    }
}
