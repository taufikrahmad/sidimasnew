<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblm_Dosen extends Model
{
    //
    protected $table = 'tblm_dosen';
    protected $searchableColumns = [
        'NIDN',
        'FirstName',
        'MiddleName',
        'LastName'
    ];
    protected $fillable = [
        'NIDN',
        'NIP',
        'FirstName',
        'MiddleName',
        'LastName',
        'FIDFakultas',
        'FIDJfa',
        'Phone',
        'Email',
        'FIDEducation',
        'BankName',
        'BankBranch',
        'BankAccountNumber',
        'FIDGrade',
        'Status',
        'CreatedBy',
        'ModifiedBy'
    ];
}
