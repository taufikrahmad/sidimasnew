<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblm_Jfa extends Model
{
    //
    protected $table = 'tblm_jfas';
    protected $fillable = [
        'Description',
        'CreatedBy',
        'ModifiedBy'
    ];
}
