<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblm_JobRole extends Model
{
    //
    protected $table = 'tblm_job_roles';
    protected $fillable = [
        'Description',
        'CreatedBy',
        'ModifiedBy'
    ];
}
