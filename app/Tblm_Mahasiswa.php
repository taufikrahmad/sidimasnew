<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblm_Mahasiswa extends Model
{
    //
    protected $searchableColumns = ['NIM','Name'];

    protected $table = 'tblm_mahasiswas';
}
