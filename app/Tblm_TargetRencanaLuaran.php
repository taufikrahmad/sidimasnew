<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblm_TargetRencanaLuaran extends Model
{
    //
    protected $table = 'tblm_target_rencana_luaran';

    protected $fillable = [
        'Description',
        'CreatedBy',
        'ModifiedBy'
    ];
}
