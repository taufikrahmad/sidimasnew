<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblm_User extends Model
{
    protected $table = 'tblm__users';

    protected $fillable = [
        'UserID','Password','FIDPersonType','CreatedBy','ModifiedBy'
    ];
}
