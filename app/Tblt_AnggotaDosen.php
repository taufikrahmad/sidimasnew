<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblt_AnggotaDosen extends Model
{
    //
    protected $table = 'tblt__anggota_dosens';

    protected $fillable = [
        'FIDProposal',
        'FIDDosen'
    ];
}
