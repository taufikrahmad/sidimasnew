<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblt_AnggotaMahasiswa extends Model
{
    //
    protected $table = 'tblt__anggota_mahasiswas';

    protected $fillable = [
        'FIDProposal',
        'FIDMahasiswa'
    ];
}
