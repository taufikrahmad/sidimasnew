<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblt_Batch extends Model
{
    protected $table = 'tblt__batches';

    protected $fillable = [
        'BatchType'
        ,'TahunAjaran'
        ,'CreatedBy'
        ,'ModifiedBy'
        ,'created_at'
        ,'updated_at'
        ,'batch_upload_proposal'
        ,'review_proposal'
        ,'pengumuman_hasil_review_proposal'
        ,'batas_upload_perbaikan'
        ,'batas_laporan_hasil'
        ,'budget1'
        ,'budget2'
    ];
}
