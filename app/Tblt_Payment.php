<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblt_Payment extends Model
{
    protected $table = 'tblt__payments';

    protected $fillable = [
        'FIDProposal','BankName','AccountNumber','TransferAmount','TransferAmount2','CreatedBy','ModifiedBy'
    ];
}
