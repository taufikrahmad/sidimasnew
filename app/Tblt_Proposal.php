<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblt_Proposal extends Model
{
    protected $table = 'tblt__proposals';

    protected $fillable = [
        'RequestorID',
        'Judul',
        'ProvinsiLokasi',
        'KabupatenLokasi',
        'KecamatanLokasi',
        'JarakLokasi',
        'Durasi',
        'SatuanDurasi',
        'proposalPDF',
        'proposalDoc',
        'PerbaikanproposalPDF',
        'PerbaikanproposalDoc',
        'DraftLaporanAkhirPDF',
        'DraftLaporanAkhirDoc',
        'LaporanAkhirPDF',
        'LaporanAkhirDoc',
        'FIDBatch',
        'CreatedBy',
        'ModifiedBy'
    ];
}
