<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblt_ReviewResult extends Model
{
    //
    protected $table = 'tblt__review_results';

    protected $fillable = [
        'FIDReviewerAssignment',
        'Remarks',
        'FIDStatus',
        'Remarks2',
        'FIDStatus2',
        'CreatedBy',
        'ModifiedBy'
    ];
}
