<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblt_ReviewerAssignment extends Model
{
    protected $table = 'tblt__reviewer_assignments';

    protected $fillable = [
        'FIDProposal','Reviewer1','Reviewer2','CreatedBy','ModifiedBy'
    ];
}
