<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tblt_UserJobRole extends Model
{
    //
    protected $table ='tblt_user_job_roles';

    protected $fillable = [
        'FIDUser',
        'FIDJobRole',
        'CreatedBy',
        'ModifiedBy'
    ];
}
