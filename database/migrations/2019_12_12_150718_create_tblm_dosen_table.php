<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblmDosenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblm_dosen', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('NIDN', 30)->unique('tblm_dosen_nidn_unique');
			$table->char('NIP', 30);
			$table->char('FirstName', 30);
			$table->char('MiddleName', 30);
			$table->char('LastName', 30);
			$table->integer('FIDFakultas');
			$table->integer('FIDJfa');
			$table->char('Phone', 20);
			$table->char('Email', 50);
			$table->integer('FIDEducation');
			$table->char('BankName', 30);
			$table->char('BankBranch', 100);
			$table->char('BankAccountNumber', 100);
			$table->integer('FIDGrade');
			$table->char('Status', 3);
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblm_dosen');
	}

}
