<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblmJobRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblm_job_roles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('Description', 100);
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
			$table->string('menubar')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblm_job_roles');
	}

}
