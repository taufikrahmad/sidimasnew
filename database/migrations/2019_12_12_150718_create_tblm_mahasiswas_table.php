<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTblmMahasiswasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblm_mahasiswas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('NIM', 30);
			$table->char('Name', 50);
			$table->char('Gender', 10);
			$table->integer('FIDProdi');
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblm_mahasiswas');
	}

}
