<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbltBatchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblt__batches', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('BatchType', 30);
			$table->char('TahunAjaran', 30);
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
			$table->date('batch_upload_proposal')->nullable();
			$table->date('review_proposal')->nullable();
			$table->date('pengumuman_hasil_review_proposal')->nullable();
			$table->date('batas_upload_perbaikan')->nullable();
			$table->date('batas_laporan_hasil')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblt__batches');
	}

}
