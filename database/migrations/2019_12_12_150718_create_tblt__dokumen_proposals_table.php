<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbltDokumenProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblt__dokumen_proposals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('FIDProposal');
			$table->integer('FIDDocumentType');
			$table->char('DocumentName', 100);
			$table->char('FileType', 20);
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblt__dokumen_proposals');
	}

}
