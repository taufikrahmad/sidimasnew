<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbltPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblt__payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('FIDProposal');
			$table->char('BankName', 30);
			$table->char('AccountNumber', 30);
			$table->decimal('TransferAmount');
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
			$table->decimal('TransferAmount2')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblt__payments');
	}

}
