<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbltProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblt__proposals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('RequestorID', 30);
			$table->char('Judul', 100);
			$table->char('ProvinsiLokasi', 100);
			$table->char('KabupatenLokasi', 100);
			$table->char('KecamatanLokasi', 100);
			$table->integer('JarakLokasi');
			$table->integer('Durasi');
			$table->char('SatuanDurasi', 30);
			$table->integer('FIDBatch');
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
			$table->char('proposalPDF', 100)->nullable();
			$table->char('proposalDoc', 100)->nullable();
			$table->char('PerbaikanproposalPDF', 100)->nullable();
			$table->char('PerbaikanproposalDoc', 100)->nullable();
			$table->char('DraftLaporanAkhirPDF', 100)->nullable();
			$table->char('DraftLaporanAkhirDoc', 100)->nullable();
			$table->char('LaporanAkhirPDF', 100)->nullable();
			$table->char('LaporanAkhirDoc', 100)->nullable();
			$table->integer('FIDLuaran')->nullable();
			$table->decimal('budget1', 10)->nullable();
			$table->decimal('budget2', 10)->nullable();
			$table->char('lembarPersetujuanPDF', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblt__proposals');
	}

}
