<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbltReviewResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblt__review_results', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('FIDReviewerAssignment');
			$table->text('Remarks');
			$table->integer('FIDStatus');
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
			$table->text('Remarks2')->nullable();
			$table->decimal('FIDStatus2', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblt__review_results');
	}

}
