<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbltSumberDanaProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblt__sumber_dana_proposals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('FIDProposal');
			$table->integer('FIDSumberDana');
			$table->decimal('Value', 30);
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblt__sumber_dana_proposals');
	}

}
