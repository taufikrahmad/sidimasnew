<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbltTargetProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblt_target_proposals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('FIDProposal');
			$table->integer('FIDTarget');
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblt_target_proposals');
	}

}
