<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbltUserJobRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblt_user_job_roles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('FIDUser');
			$table->integer('FIDJobRole');
			$table->char('CreatedBy', 30);
			$table->char('ModifiedBy', 30);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblt_user_job_roles');
	}

}
