<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblmUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblm__users', function(Blueprint $table)
		{
			//$table->foreign('"FIDPersonType"', 'tblm__users_fidpersontype_foreign')->references('id')->on('tblm__person_types')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblm__users', function(Blueprint $table)
		{
			//$table->dropForeign('tblm__users_fidpersontype_foreign');
		});
	}

}
