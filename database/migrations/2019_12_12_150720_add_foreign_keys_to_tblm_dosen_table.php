<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblmDosenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblm_dosen', function(Blueprint $table)
		{
			$table->foreign('"FIDFakultas"', 'tblm_dosen_fidfakultas_foreign')->references('id')->on('tblm_fakultas')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDJfa"', 'tblm_dosen_fidjfa_foreign')->references('id')->on('tblm_jfas')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDEducation"', 'tblm_dosen_fideducation_foreign')->references('id')->on('tblm_education_levels')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDGrade"', 'tblm_dosen_fidgrade_foreign')->references('id')->on('tblm_grade_dosens')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblm_dosen', function(Blueprint $table)
		{
			$table->dropForeign('tblm_dosen_fidfakultas_foreign');
			$table->dropForeign('tblm_dosen_fidjfa_foreign');
			$table->dropForeign('tblm_dosen_fideducation_foreign');
			$table->dropForeign('tblm_dosen_fidgrade_foreign');
		});
	}

}
