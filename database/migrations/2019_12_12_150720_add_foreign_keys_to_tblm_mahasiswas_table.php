<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblmMahasiswasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblm_mahasiswas', function(Blueprint $table)
		{
			$table->foreign('"FIDProdi"', 'tblm_mahasiswas_fidprodi_foreign')->references('id')->on('tblm_prodis')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblm_mahasiswas', function(Blueprint $table)
		{
			$table->dropForeign('tblm_mahasiswas_fidprodi_foreign');
		});
	}

}
