<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblmProdisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblm_prodis', function(Blueprint $table)
		{
			$table->foreign('"FIDFakultas"', 'tblm_prodis_fidfakultas_foreign')->references('id')->on('tblm_fakultas')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblm_prodis', function(Blueprint $table)
		{
			$table->dropForeign('tblm_prodis_fidfakultas_foreign');
		});
	}

}
