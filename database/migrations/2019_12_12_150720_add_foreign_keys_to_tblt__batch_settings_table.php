<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltBatchSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt__batch_settings', function(Blueprint $table)
		{
			$table->foreign('"FIDBatch"', 'tblt__batch_settings_fidbatch_foreign')->references('id')->on('tblt__batches')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDSetting"', 'tblt__batch_settings_fidsetting_foreign')->references('id')->on('tblm_batch_setting_param')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt__batch_settings', function(Blueprint $table)
		{
			$table->dropForeign('tblt__batch_settings_fidbatch_foreign');
			$table->dropForeign('tblt__batch_settings_fidsetting_foreign');
		});
	}

}
