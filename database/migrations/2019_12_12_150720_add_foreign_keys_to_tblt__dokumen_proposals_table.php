<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltDokumenProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt__dokumen_proposals', function(Blueprint $table)
		{
			$table->foreign('"FIDProposal"', 'tblt__dokumen_proposals_fidproposal_foreign')->references('id')->on('tblt__proposals')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDDocumentType"', 'tblt__dokumen_proposals_fiddocumenttype_foreign')->references('id')->on('tblm__document_types')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt__dokumen_proposals', function(Blueprint $table)
		{
			$table->dropForeign('tblt__dokumen_proposals_fidproposal_foreign');
			$table->dropForeign('tblt__dokumen_proposals_fiddocumenttype_foreign');
		});
	}

}
