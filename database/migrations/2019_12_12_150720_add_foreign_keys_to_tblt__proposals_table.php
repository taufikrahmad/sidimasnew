<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt__proposals', function(Blueprint $table)
		{
			$table->foreign('"RequestorID"', 'tblt__proposals_requestorid_foreign')->references('"NIDN"')->on('tblm_dosen')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDBatch"', 'tblt__proposals_fidbatch_foreign')->references('id')->on('tblt__batches')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt__proposals', function(Blueprint $table)
		{
			$table->dropForeign('tblt__proposals_requestorid_foreign');
			$table->dropForeign('tblt__proposals_fidbatch_foreign');
		});
	}

}
