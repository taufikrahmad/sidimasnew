<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltReviewResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt__review_results', function(Blueprint $table)
		{
			$table->foreign('"FIDReviewerAssignment"', 'tblt__review_results_fidreviewerassignment_foreign')->references('id')->on('tblt__proposals')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDStatus"', 'tblt__review_results_fidstatus_foreign')->references('id')->on('tblm_statuses')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt__review_results', function(Blueprint $table)
		{
			$table->dropForeign('tblt__review_results_fidreviewerassignment_foreign');
			$table->dropForeign('tblt__review_results_fidstatus_foreign');
		});
	}

}
