<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltReviewerAssignmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt__reviewer_assignments', function(Blueprint $table)
		{
			$table->foreign('"FIDProposal"', 'tblt__reviewer_assignments_fidproposal_foreign')->references('id')->on('tblt__proposals')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt__reviewer_assignments', function(Blueprint $table)
		{
			$table->dropForeign('tblt__reviewer_assignments_fidproposal_foreign');
		});
	}

}
