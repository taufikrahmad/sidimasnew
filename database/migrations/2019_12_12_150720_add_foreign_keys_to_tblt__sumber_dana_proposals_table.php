<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltSumberDanaProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt__sumber_dana_proposals', function(Blueprint $table)
		{
			$table->foreign('"FIDProposal"', 'tblt__sumber_dana_proposals_fidproposal_foreign')->references('id')->on('tblt__proposals')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDSumberDana"', 'tblt__sumber_dana_proposals_fidsumberdana_foreign')->references('id')->on('tblm_sumber_dana')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt__sumber_dana_proposals', function(Blueprint $table)
		{
			$table->dropForeign('tblt__sumber_dana_proposals_fidproposal_foreign');
			$table->dropForeign('tblt__sumber_dana_proposals_fidsumberdana_foreign');
		});
	}

}
