<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltUserApplicationRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt__user_application_roles', function(Blueprint $table)
		{
			$table->foreign('"FIDUser"', 'tblt__user_application_roles_fiduser_foreign')->references('id')->on('tblm__users')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDApplicationRole"', 'tblt__user_application_roles_fidapplicationrole_foreign')->references('id')->on('tblm_application_roles')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt__user_application_roles', function(Blueprint $table)
		{
			$table->dropForeign('tblt__user_application_roles_fiduser_foreign');
			$table->dropForeign('tblt__user_application_roles_fidapplicationrole_foreign');
		});
	}

}
