<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltTargetProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt_target_proposals', function(Blueprint $table)
		{
			$table->foreign('"FIDProposal"', 'tblt_target_proposals_fidproposal_foreign')->references('id')->on('tblt__proposals')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDTarget"', 'tblt_target_proposals_fidtarget_foreign')->references('id')->on('tblm_target_rencana_luaran')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt_target_proposals', function(Blueprint $table)
		{
			$table->dropForeign('tblt_target_proposals_fidproposal_foreign');
			$table->dropForeign('tblt_target_proposals_fidtarget_foreign');
		});
	}

}
