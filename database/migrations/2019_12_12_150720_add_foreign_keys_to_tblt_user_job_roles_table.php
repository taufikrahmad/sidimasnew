<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTbltUserJobRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblt_user_job_roles', function(Blueprint $table)
		{
			$table->foreign('"FIDUser"', 'tblt_user_job_roles_fiduser_foreign')->references('id')->on('tblm__users')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('"FIDJobRole"', 'tblt_user_job_roles_fidjobrole_foreign')->references('id')->on('tblm_job_roles')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblt_user_job_roles', function(Blueprint $table)
		{
			$table->dropForeign('tblt_user_job_roles_fiduser_foreign');
			$table->dropForeign('tblt_user_job_roles_fidjobrole_foreign');
		});
	}

}
