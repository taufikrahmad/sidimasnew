<?php

use Illuminate\Database\Seeder;

class TblDosen extends Seeder
{
    public function randomName() {
        $names = array(
            'Juan',
            'Luis',
            'Pedro',
            'Erick',
            'Sebastian',
            'Timo',
            'Rafael'
            // and so on
    
        );
        return $names[rand ( 0 , count($names) -1)];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = array(
            'Juan',
            'Luis',
            'Pedro',
            'Erick',
            'Sebastian',
            'Timo',
            'Rafael'
            // and so on
    
        );
        //
        for($i = 1;$i<=30;$i++){
        DB::table('tblm_dosen')->insert([
            'NIDN' => '418181101'.($i<10 ? '0'.$i : $i),
            'NIP' => '418181101'.($i<10 ? '0'.$i : $i),
            'FirstName' => $names[rand ( 0 , count($names) -1)],
            'MiddleName' => $names[rand ( 0 , count($names) -1)],
            'LastName' => $names[rand ( 0 , count($names) -1)],
            'FIDFakultas' => 5,
            'FIDJfa' => 2,
            'Phone' => Str::random(20),
            'Email' => Str::random(30),
            'FIDEducation' => 2,
            'BankName' => Str::random(30),
            'BankBranch' => Str::random(30),
            'BankAccountNumber' => Str::random(30),
            'FIDGrade' => 2,
            'Status' => 1,
            'CreatedBy' => 'system',
            'ModifiedBy' => 'system'
        ]);
        }
    }
}
