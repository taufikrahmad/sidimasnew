<?php

use Illuminate\Database\Seeder;

class TblMahasiswa extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $names = array(
            'Juan',
            'Luis',
            'Pedro',
            'Erick',
            'Sebastian',
            'Timo',
            'Rafael'
            // and so on
    
        );
        $gender = array(
            'Laki-Laki',
            'Perempuan'
            // and so on
    
        );
        //
        for($i = 1;$i<=30;$i++){
        DB::table('tblm_mahasiswas')->insert([
            'NIM' => '519181101'.($i<10 ? '0'.$i : $i),
            'Name' => $names[rand ( 0 , count($names) -1)],
            'Gender' => $gender[rand ( 0 , count($gender) -1)],
            'FIDProdi' => 2,
            'CreatedBy' => 'system',
            'ModifiedBy' => 'system'
        ]);
        }
    }
}
