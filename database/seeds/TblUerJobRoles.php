<?php

use Illuminate\Database\Seeder;

class TblUerJobRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 7;$i<=22;$i++){
            DB::table('tblt_user_job_roles')->insert([
                'FIDUser' => $i,
                'FIDJobRole' => 4,
                'CreatedBy' => 'system',
                'ModifiedBy' => 'system'
            ]);
        }
        for($i = 23;$i<=37;$i++){
            DB::table('tblt_user_job_roles')->insert([
                'FIDUser' => $i,
                'FIDJobRole' => 5,
                'CreatedBy' => 'system',
                'ModifiedBy' => 'system'
            ]);
        }
    }
}
