<?php

use Illuminate\Database\Seeder;

class TblUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 1;$i<=30;$i++){
            DB::table('tblm__users')->insert([
                'UserID' => '418181101'.($i<10 ? '0'.$i : $i),
                'Password' => '$2y$10$NRfJ2.ZP7Sfo5UIqUCvToOlWVNbXJdRjPuFvkhGBDHMA/BNU8b2sW',
                'FIDPersonType' => 1,
                'CreatedBy' => 'system',
                'ModifiedBy' => 'system'
            ]);
            }
    }
}
