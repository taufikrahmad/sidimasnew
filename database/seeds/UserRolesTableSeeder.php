<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Tblt_UserJobRole::create(
            /*[
                'FIDUser'  => 38,
                'FIDJobRole'  => 4,
                'CreatedBy'  => 'system',
                'ModifiedBy'  => 'system'
            ],
            [
                'FIDUser'  => 39,
                'FIDJobRole'  => 5,
                'CreatedBy'  => 'system',
                'ModifiedBy'  => 'system'
            ],*/
            [
                'FIDUser'  => 40,
                'FIDJobRole'  => 7,
                'CreatedBy'  => 'system',
                'ModifiedBy'  => 'system'
            ]
        );
    }
}
