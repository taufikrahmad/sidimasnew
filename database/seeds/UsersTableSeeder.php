<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Tblm_User::create(
            [
                'UserID'  => '41818110103',
                'Password'  => Hash::make('12345'),
                'FIDPersonType'  => 1,
                'CreatedBy'  => 'system',
                'ModifiedBy'  => 'system'
            ]
        );
    }
}
