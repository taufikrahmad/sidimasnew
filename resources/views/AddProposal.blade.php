@extends('main')

@section('title','| Upload Proposal ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Create Proposal
        <small>Create Proposal</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Create Proposal</a></li>
        <li class="active">Create</li>
      </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Create Proposal</h3>
          <div class="box-tools pull-right">
            &nbsp;
          </div>
        </div>
        <div class="box-body">
            <form action="{{url('/add-proposal')}}" id="formConfiguration" enctype="multipart/form-data" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Batch</label>
                            <div class="col-md-4 col-sm-12">                                
                                <select id="FIDBatch" name="FIDBatch" class="form-control select2">
                                        <option value=""> -- Select Batch -- </option>
                                        @foreach($modelBatch as $data)
                                        <option value="{{$data->id}}">{{$data->BatchType}} - {{$data->TahunAjaran}}</option>                                        
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchType" class="col-sm-2 control-label">Judul</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" name="Judul" id="Judul" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Provinsi Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" name="ProvinsiLokasi" id="ProvinsiLokasi" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Kabupaten Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" name="KabupatenLokasi" id="KabupatenLokasi" class="form-control" />
                            </div>
                            </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Kecamatan Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" name="KecamatanLokasi" id="KecamatanLokasi" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Jarak Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" name="JarakLokasi" id="JarakLokasi" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Durasi</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" name="Durasi" id="Durasi" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Satuan Durasi</label>
                            <div class="col-md-4 col-sm-12">
                                <select id="SatuanDurasi" name="SatuanDurasi" class="form-control select2">
                                        <option value=""> -- Select Satuan Durasi -- </option>
                                        <option value="hari"> Hari </option>
                                        <option value="bulan"> Bulan </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="targetLuaran" class="col-sm-2 control-label">Target Luaran</label>
                            <div class="col-md-4 col-sm-12">
                                <select id="targetLuaran" name="targetLuaran" class="form-control select2">
                                        <option value=""> -- Select Target Luaran -- </option>
                                        @foreach($modelLuaran as $data)
                                        <option value="{{$data->id}}">{{trim($data->Description)}}</option>                                        
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        @foreach ($requiredDocs as $requiredDoc)
                        <div class="form-group">
                            <label for="{{$requiredDoc['id']}}" class="col-sm-2 control-label">{{$requiredDoc['desc']}}</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="file" accept="application/msword,
                                    application/vnd.openxmlformats-officedocument.wordprocessingml.document" id="{{$requiredDoc['id']}}" name="{{$requiredDoc['id']}}" class="form-control">
                            </div>
                        </div>
                        @endforeach
                        <div class="form-group">
                            <label for="budget1" class="col-sm-2 control-label">Budget 1</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="number" name="budget1" id="budget1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="budget2" class="col-sm-2 control-label">Budget 2</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="number" name="budget2" id="budget2" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="budget2" class="col-sm-2 control-label">Anggota Mahasiswa</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="hidden" name="selectedMahasiswa" name="selectedMahasiswa" value="51918110101,51918110102" id="selectedMahasiswa" class="form-control" />
                                <select id="ddlMahasiswa" name="ddlMahasiswa" class="form-control" multiple></select>
                            </div>
                        </div>
                        <div class="form-group">
                                <label for="budget2" class="col-sm-2 control-label">Anggota Dosen</label>
                                <div class="col-md-4 col-sm-12">
                                    <input type="hidden" name="selectedDosen" id="selectedDosen" id="selectedMahasiswa" class="form-control" />
                                    <select id="ddlDosen" name="ddlDosen[]" class="form-control" multiple></select>
                                </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2 col-sm-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

$(function() {

    let selections = [];

    var extract_preselected_ids = function(element){
        var preselected_ids = [];
        if(element.val())
            $(element.val().split(",")).each(function () {
                preselected_ids.push({id: this});
            });
        console.log(preselected_ids);
        return preselected_ids;
    };

    var preselect = function(preselected_ids){
        var pre_selections = [];



        for(index in selections)
            for(id_index in preselected_ids)
                if (selections[index].id == preselected_ids[id_index].id)
                    pre_selections.push(selections[index]);
        return pre_selections;
    };

    $('#ddlMahasiswa').select2({
        placeholder: "Masukkan 3 Karakter NIM / Nama",
        minimumInputLength: 3,
        multiple: true,
        ajax: {
            url: '/mahasiswa/find',
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        initSelection: function (element, callback) {
            var preselected_ids = extract_preselected_ids($("#selectedMahasiswa")); //1,3,4 are the pre-selected IDs as per HTML attributes
              var preselections = preselect(preselected_ids);
            callback(preselections);
        }
    })
    .on('change', function (e) {
        $("#selectedMahasiswa").val($("#ddlMahasiswa").select2('val').join(','));
    }).on('select', function (e) {
        console.log("select");
    });    


    $('#ddlDosen').select2({
        placeholder: "Masukkan 3 Karakter NIP / Nama",
        minimumInputLength: 3,
        multiple: true,
        ajax: {
            url: '/dosen/find',
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    })
    .on('change', function (e) {
        $("#selectedDosen").val($("#ddlDosen").select2('val').join(','));
    }).on('select', function (e) {
        console.log("select");
    });
});
</script>
@endsection
