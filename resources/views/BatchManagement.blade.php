@extends('main')

@section('title','| Configurations ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Batch
        <small>Batch</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Batch</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Batch</h3>
          <div class="box-tools pull-right">
            <a href="{{url('/add-batch-management')}}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Add</a>
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Batch Type</th>
                                <th>Batch Upload Proposal</th>
                                <th>Review Proposal</th>
                                <th>Pengumuman Hasil Review Proposal</th>
                                <th>Batas Upload Perbaikan</th>
                                <th>Batas Laporan Hasil</th>
                                <th>Edit / Delete</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

function editConfig(e){
    window.location.assign("{{url('Configuration/edit')}}/"+$(e).data('target'));
}


$(function() {

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/datatables-batch',
        columns: [
            { data: 'BatchType', name: 'BatchType' },
            { data: 'batch_upload_proposal', name: 'batch_upload_proposal' },
            { data: 'review_proposal', name: 'review_proposal' },
            { data: 'pengumuman_hasil_review_proposal', name: 'pengumuman_hasil_review_proposal' },
            { data: 'batas_upload_perbaikan', name: 'batas_upload_perbaikan' },
            { data: 'batas_laporan_hasil', name: 'batas_laporan_hasil' },
            {{-- { data: 'id', name: 'id' }, --}}
            {
                data: 'id',
                className: "center"
            }
        ],
        "columnDefs": [ {
            "targets": 6,
            "searchable": false,
            "render": function ( data, type, row ) {
                    return '<a href="{{url('/edit-batch-management/')}}/'+data+'" class="editor_edit">Edit</a> / <a href="{{url('/delete-batch-management/')}}/'+data+'" class="editor_remove">Delete</a>';
                }
        } ]
    });



});
</script>
@endsection
