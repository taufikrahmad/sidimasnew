@extends('main')

@section('title','| Edit Assignment ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Assignment
        <small>Edit Assignment</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Edit Assignment</a></li>
        <li class="active">Edit</li>
      </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Edit Assignment</h3>
          <div class="box-tools pull-right">
            &nbsp;
          </div>
        </div>
        <div class="box-body">
            <form action="{{url('/edit-assignment/')}}/{{$assignment->FIDProposal}}" id="formConfiguration" enctype="multipart/form-data" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Batch</label>
                            <div class="col-md-4 col-sm-12">                                
                                <input type="text" readonly class="form-control" name="BatchName" id="BatchName" value="{{$assignment->BatchName}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchType" class="col-sm-2 control-label">Judul</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly name="Judul" id="Judul" class="form-control" value="{{$assignment->Judul}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Provinsi Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" readonly name="ProvinsiLokasi" id="ProvinsiLokasi" class="form-control" value="{{$assignment->Provinsi}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Kabupaten Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" readonly name="KabupatenLokasi" id="KabupatenLokasi" class="form-control" value="{{$assignment->Kabupaten}}"/>
                            </div>
                            </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Kecamatan Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly name="KecamatanLokasi" id="KecamatanLokasi" class="form-control" value="{{$assignment->Kecamatan}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Jarak Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly name="JarakLokasi" id="JarakLokasi" class="form-control" value="{{$assignment->JarakLokasi}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Durasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly name="Durasi" id="Durasi" class="form-control" value="{{$assignment->Durasi}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Satuan Durasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly name="SatuanDurasi" id="SatuanDurasi" class="form-control" value="{{$assignment->SatuanDurasi}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="targetLuaran" class="col-sm-2 control-label">Reviewer 1</label>
                            <div class="col-md-4 col-sm-12">
                                <select id="targetLuaran" name="Reviewer1" id="Reviewer1" class="form-control select2">
                                        <option value=""> -- Select Reviewer 1 -- </option>
                                        @foreach($reviewer1 as $data)
                                        <option {{(trim($assignment->Reviewer1) == trim($data->NIP) ? "selected" : "")}} value="{{trim($data->NIP)}}">{{trim($data->FirstName)}} {{trim($data->MiddleName)}} {{trim($data->LastName)}} [{{$data->NIP}}]</option>                                        
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="targetLuaran" class="col-sm-2 control-label">Reviewer 2</label>
                            <div class="col-md-4 col-sm-12">
                                <select id="targetLuaran" name="Reviewer2" id="Reviewer2" class="form-control select2">
                                    <option value=""> -- Select Reviewer 2 -- </option>
                                    @foreach($reviewer2 as $data)
                                    <option {{(trim($assignment->Reviewer2) == trim($data->NIP) ? "selected" : "")}} value="{{trim($data->NIP)}}">{{trim($data->FirstName)}} {{trim($data->MiddleName)}} {{trim($data->LastName)}} [{{$data->NIP}}]</option>                                        
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2 col-sm-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

$(function() {
    
});
</script>
@endsection
