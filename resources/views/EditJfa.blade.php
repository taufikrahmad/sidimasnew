@extends('main')

@section('title','| Edit Jfa')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Jfa
        <small>Edit Jfa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="#">Jfa</a></li>
        <li class="active">Edit</li>
      </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Edit Jfa</h3>
          <div class="box-tools pull-right">
            &nbsp;
          </div>
        </div>
        <div class="box-body">
        <form action="{{url('/edit-jfa')}}/{{$ModelJfa->id}}" id="formTarget" name="formTarget" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description</label>
                            <div class="col-md-4 col-sm-12">
                            <input type="text" class="form-control required" value="{{trim($ModelJfa->Description)}}" name="description" id="description"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2 col-sm-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

$(function() {
    
});
</script>
@endsection
