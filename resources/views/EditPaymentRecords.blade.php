@extends('main')

@section('title','| Edit Payment Records ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Payment Records
        <small>Edit Payment Records</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Edit Payment Records</a></li>
        <li class="active">Edit</li>
      </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Edit Payment Records</h3>
          <div class="box-tools pull-right">
            &nbsp;
          </div>
        </div>
        <div class="box-body">
            <form action="{{url('/edit-payment-records/')}}/{{$paymentRecords->FIDProposal}}" id="formConfiguration" enctype="multipart/form-data" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="batchType" class="col-sm-2 control-label">Judul</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly name="Judul" id="Judul" class="form-control" value="{{$paymentRecords->Judul}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Nama Bank</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" name="BankName" id="BankName" class="form-control" value="{{$paymentRecords->BankName}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Nomor Account</label>
                            <div class="col-md-4 col-sm-12">
                                    <input type="text" name="AccountNumber" id="AccountNumber" class="form-control" value="{{$paymentRecords->AccountNumber}}"/>
                            </div>
                            </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Transfer 1</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" name="TransferAmount" id="TransferAmount" class="form-control" value="{{$paymentRecords->TransferAmount}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Transfer 2</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" name="TransferAmount2" id="TransferAmount2" class="form-control" value="{{$paymentRecords->TransferAmount2}}"/>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2 col-sm-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

$(function() {
    
});
</script>
@endsection
