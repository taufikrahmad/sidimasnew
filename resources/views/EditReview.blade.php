@extends('main')

@section('title','| Review ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Proposal
        <small>Edit Review</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Review</a></li>
        <li class="active">Edit</li>
      </ol>
</section>
<?php
    $status = 0;
    if(count($dataReviewResult) > 0){
        if($dataJob == 'Reviewer 1'){
            $status = $dataReviewResult[0]->FIDStatus;
        }else{
            $status = $dataReviewResult[0]->FIDStatus2;
        }
    }
?>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Edit Review</h3>
          <div class="box-tools pull-right">
            &nbsp;
          </div>
        </div>
        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Data Proposal</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Anggota</a></li>
                  <li><a href="#tab_3" data-toggle="tab">Dokumen Pendukung</a></li>
                  <li><a href="#tab_4" data-toggle="tab">Result</a></li>
                  @if (Session::get('isAdmin'))
                  <li><a href="#tab_5" data-toggle="tab">History</a></li>
                  @endif
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="batchType" class="col-sm-2 control-label">Batch</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly class="form-control" name="batchType" id="batchType"
                            value="{{trim($dataProposal[0]->BatchType).' '.trim($dataProposal[0]->TahunAjaran)}}" />
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="Judul" class="col-sm-2 control-label">Judul</label>
                            <div class="col-md-4 col-sm-12">
                                <textarea name="Judul" id="Judul" class="form-control" rows="3" style="resize:vertical" readonly>{{trim($dataProposal[0]->Judul)}}</textarea>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="provinsi" class="col-sm-2 control-label">Provinsi Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly class="form-control" name="provinsi" id="provinsi"
                            value="{{trim($dataProposal[0]->ProvinsiLokasi)}}" />
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="kabupaten" class="col-sm-2 control-label">Kabupaten Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly class="form-control" name="kabupaten" id="kabupaten"
                            value="{{trim($dataProposal[0]->KabupatenLokasi)}}" />
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="jarak" class="col-sm-2 control-label">Jarak Lokasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly class="form-control" name="jarak" id="jarak"
                            value="{{trim($dataProposal[0]->JarakLokasi)}}" />
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="durasi" class="col-sm-2 control-label">Durasi</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly class="form-control" name="durasi" id="durasi"
                            value="{{trim($dataProposal[0]->Durasi.' '.trim($dataProposal[0]->SatuanDurasi))}}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="target" class="col-sm-2 control-label">Target Luaran</label>
                            <div class="col-md-4 col-sm-12">
                                <textarea name="target" id="target" class="form-control" rows="3" style="resize:vertical" readonly>{{trim($dataProposal[0]->RencanaLuaran)}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="budget" class="col-sm-2 control-label">Budget</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" readonly class="form-control" name="budget" id="budget"
                            value="{{number_format(($dataProposal[0]->budget1+$dataProposal[0]->budget2),2)}}" />
                            </div>
                        </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    <b>Dosen :</b>
                    <table class="table table-hover">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIDN</th>
                        </tr>
                        @foreach($dataAnggotaDosen as $data)
                        <tr>
                            <td>
                                {{$loop->iteration}}
                            </td>
                            <td>
                                {{trim($data->FirstName).' '.trim($data->MiddleName).' '.trim($data->LastName)}}
                            </td>
                            <td>
                                {{trim($data->NIDN)}}
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <b>Mahasiswa :</b>
                    <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIM</th>
                    </tr>
                    @foreach($dataAnggotaMahasiswa as $data)
					<tr>
						<td>
						    {{$loop->iteration}}
						</td>
                        <td>
                            {{trim($data->Name)}}
                        </td>
                        <td>
                            {{trim($data->NIM)}}
                        </td>
					</tr>
					@endforeach
                    </table>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                    <div class="form-horizontal">
                            @foreach ($requiredDocs as $requiredDoc)
                            <div class="form-group">
                                <label for="{{$requiredDoc['id']}}" class="col-md-3 col-sm-12 control-label">{{$requiredDoc['desc']}}</label>
                                <div class="col-md-6 col-sm-12">
                                    @switch($requiredDoc['id'])
                                        @case("proposalPDF")
                                            @if(trim($dataProposal[0]->proposalPDF) != '')
                                                <a class="btn btn-sm" href="{{url('/download-doc-proposal')}}/{{$dataProposal[0]->proposalPDF}}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            @endif
                                            @break
                                        @case("proposalDoc")
                                            @if(trim($dataProposal[0]->proposalDoc) != '')
                                                <a class="btn btn-sm" href="{{url('/download-doc-proposal')}}/{{$dataProposal[0]->proposalDoc}}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            @endif
                                            @break
                                        @case("PerbaikanproposalPDF")
                                            @if(trim($dataProposal[0]->PerbaikanproposalPDF) != '')
                                                <a class="btn btn-sm" href="{{url('/download-doc-proposal')}}/{{$dataProposal[0]->PerbaikanproposalPDF}}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            @endif
                                            @break
                                        @case("PerbaikanproposalDoc")
                                            @if(trim($dataProposal[0]->PerbaikanproposalDoc) != '')
                                                <a class="btn btn-sm" href="{{url('/download-doc-proposal')}}/{{$dataProposal[0]->PerbaikanproposalDoc}}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            @endif
                                            @break
                                        @case("DraftLaporanAkhirPDF")
                                            @if(trim($dataProposal[0]->DraftLaporanAkhirPDF) != '')
                                                <a class="btn btn-sm" href="{{url('/download-doc-proposal')}}/{{$dataProposal[0]->DraftLaporanAkhirPDF}}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            @endif
                                            @break
                                        @case("DraftLaporanAkhirDoc")
                                            @if(trim($dataProposal[0]->DraftLaporanAkhirDoc) != '')
                                                <a class="btn btn-sm" href="{{url('/download-doc-proposal')}}/{{$dataProposal[0]->DraftLaporanAkhirDoc}}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            @endif
                                            @break
                                        @case("LaporanAkhirPDF")
                                            @if(trim($dataProposal[0]->LaporanAkhirPDF) != '')
                                                <a class="btn btn-sm" href="{{url('/download-doc-proposal')}}/{{$dataProposal[0]->LaporanAkhirPDF}}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            @endif
                                            @break
                                        @case("LaporanAkhirDoc")
                                            @if(trim($dataProposal[0]->LaporanAkhirDoc) != '')
                                                <a class="btn btn-sm" href="{{url('/download-doc-proposal')}}/{{$dataProposal[0]->LaporanAkhirDoc}}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                            @endif
                                            @break
                                    @endswitch
                                </div>
                            </div>
                            @endforeach
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_4">
                  <form action="{{url('/edit-review')}}/{{$dataAssignment[0]->id}}" id="formConfiguration" class="form-horizontal" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="role" class="col-sm-2 control-label">Your Role</label>
                                <div class="col-md-4 col-sm-12">
                                    <input type="text" readonly class="form-control" name="role" id="role"
                                value="{{$dataJob}}" />
                                <input type="hidden" readonly class="form-control" name="resultID" id="resultID"
                                value="{{(count($dataReviewResult) > 0 ? $dataReviewResult[0]->id : 0)}}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarks" class="col-sm-2 control-label">Remarks</label>
                                <div class="col-md-4 col-sm-12">
                                    <textarea name="remarks" id="remarks" class="form-control" rows="3" style="resize:vertical">{{(count($dataReviewResult) > 0 ? ($dataJob == 'Reviewer 1' ? $dataReviewResult[0]->Remarks : $dataReviewResult[0]->Remarks2) : '')}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status" class="col-sm-2 control-label">Status</label>
                                <div class="col-md-4 col-sm-12">
                                    <select name="status" id="status" class="form-control">
                                        <option value=""> [Choose Status] </option>
                                        <option value="1" {{$status == 1 ? 'selected' : ''}}> Approve </option>
                                        <option value="3" {{$status == 3 ? 'selected' : ''}}> Reject </option>
                                    </select>
                                </div>
                            </div>
                            @if (!Session::get('isAdmin'))
                            <div class="form-group">
                                <label for="remarks" class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-md-4 col-sm-12">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </div>
                            @endif
                        </form>
                  </div>
                  @if (Session::get('isAdmin'))
                  <div class="tab-pane" id="tab_5">
                        <table class="table table-hover">
                            <tr>
                                <th>&nbsp;</th>
                                <th>Remarks</th>
                                <th>Status</th>
                            </tr>
                            <tr>
                                <td>Reviewer 1</td>
                                <td>{{(count($dataReviewResult) > 0 ? $dataReviewResult[0]->Remarks : '')}}</td>
                                <td>{{(count($dataReviewResult) > 0 ? ($dataReviewResult[0]->FIDStatus == 1 ? 'Approved' : 'Reject') : '')}}</td>
                            </tr>
                            <tr>
                                <td>Reviewer 2</td>
                                <td>{{(count($dataReviewResult) > 0 ? $dataReviewResult[0]->Remarks2 : ($dataReviewResult[0]->FIDStatus == 3 ? 'Reject' : ''))}}</td>
                                <td>{{(count($dataReviewResult) > 0 ? ($dataReviewResult[0]->FIDStatus2 == 1 ? 'Approved' : ($dataReviewResult[0]->FIDStatus2 == 3 ? 'Reject' : '')) : '')}}</td>
                            </tr>
                        </table>
                  </div>
                  @endif
                </div>
                <!-- /.tab-content -->
                
              </div>
              <!-- nav-tabs-custom -->
        </div>
        
    </div>
    
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">
</script>
@endsection
