@extends('main')

@section('title','| Master Target Luaran ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Target Luaran
        <small>Target Luaran</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li class="active">Target Luaran</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Batch</h3>
          <div class="box-tools pull-right">
            <a href="{{url('/add-master-target')}}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Add</a>
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered" id="table-target">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Deskripsi</th>
                                <th>Edit / Delete</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

function editConfig(e){
    window.location.assign("{{url('Configuration/edit')}}/"+$(e).data('target'));
}


$(function() {

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $('#table-target').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/datatables-mtarget',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'Description', name: 'Description' },
            {{-- { data: 'id', name: 'id' }, --}}
            {
                data: 'id',
                className: "center"
            }
        ],
        "columnDefs": [ {
            "targets": 2,
            "searchable": false,
            "render": function ( data, type, row ) {
                    return '<a href="{{url('/edit-master-target/')}}/'+data+'" class="editor_edit">Edit</a> / <a href="{{url('/delete-master-target/')}}/'+data+'" class="editor_remove">Delete</a>';
                }
        } ]
    });



});
</script>
@endsection
