@extends('main')

@section('title','| Assignment ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Payment Records
        <small>Payment Records</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Payment Records</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> List Payment Records</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Durasi</th>
                                <th>Requestor</th>
                                <th>Pembayan 1</th>
                                <th>Pembayaran 2</th>
                                <th>Created By</th>
                                <th>Modified By</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

function editConfig(e){
    window.location.assign("{{url('Configuration/edit')}}/"+$(e).data('target'));
}


$(function() {

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/datatables-payment-records',
        columns: [
            { data: 'Judul', name: 'Judul' },
            { data: 'Durasi', name: 'Durasi' },
            { data: 'FirstName', name: 'FirstName' },
            { data: 'TransferAmount', name: 'TransferAmount' },
            { data: 'TransferAmount2', name: 'TransferAmount2' },
            { data: 'CreatedBy', name: 'CreatedBy' },
            { data: 'ModifiedBy', name: 'ModifiedBy' },
            { data: 'FIDProposal', name: 'FIDProposal'}
        ],
        columnDefs: [
                {
                    "targets": [0,5,6],
                    "render": function (data, type, row) { return (data != null ? data.toString().trim() : ''); }
                },
                {
                    "targets": [1],
                    "render": function (data, type, row) { return (row['Durasi'] != null && row['SatuanDurasi']  ? row['Durasi'].toString().trim() + ' ' + row['SatuanDurasi'].toString().trim() : ''); }
                },
                {
                    "targets": [2],
                    "render": function (data, type, row) { return (row['FirstName'] != null && row['MiddleName'] && row['LastName'] ? row['FirstName'].toString().trim() +' '+ row['MiddleName'].toString().trim() +' '+ row['LastName'].toString().trim() : ''); }
                },
                {
                    "targets": 7,
                    "searchable": false,
                    "render": function ( data, type, row ) {
                        return '<a href="{{url('/edit-payment-records/')}}/'+data+'" class="editor_edit">Edit</a>';
                    }
                }
        ]
    });



});
</script>
@endsection
