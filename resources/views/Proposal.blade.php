@extends('main')

@section('title','| Proposal ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Proposal
        <small>Upload Proposal</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Proposal</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> List Proposal</h3>
          <div class="box-tools pull-right">
            <a href="{{url('/add-proposal')}}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Add</a>
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Requestor ID</th>
                                <th>Judul</th>
                                <th>Provinsi Lokasi</th>
                                <th>Kabupaten Lokasi</th>
                                <th>Kecamatan Lokasi</th>
                                <th>Jarak Lokasi (km)</th>
                                <th>Durasi</th>
                                <th>Satuan Durasi</th>
                                <th>FID Batch</th>
                                <th>Status</th>
                                <th>SK</th>
                                <th>Created By</th>
                                <th>Modified By</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

function editConfig(e){
    window.location.assign("{{url('Configuration/edit')}}/"+$(e).data('target'));
}


$(function() {
    $("body").addClass('sidebar-collapse');

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/datatables-proposal',
        columns: [
            { data: 'RequestorID', name: 'RequestorID' },
            { data: 'Judul', name: 'Judul' },
            { data: 'ProvinsiLokasi', name: 'ProvinsiLokasi' },
            { data: 'KabupatenLokasi', name: 'KabupatenLokasi' },
            { data: 'KecamatanLokasi', name: 'KecamatanLokasi' },
            { data: 'JarakLokasi', name: 'JarakLokasi' },
            { data: 'Durasi', name: 'Durasi' },
            { data: 'SatuanDurasi', name: 'SatuanDurasi' },
            { data: 'FIDBatch', name: 'FIDBatch' },
            { data: 'status', name: 'status' },
            { data: 'status', name: 'status' },
            { data: 'CreatedBy', name: 'CreatedBy' },
            { data: 'ModifiedBy', name: 'ModifiedBy' },
            {{-- { data: 'id', name: 'id' }, --}}
            {
                data: 'id',
                className: "center"
            }
        ],
        "columnDefs": [ 
        {
            "targets": 10,
            "searchable": false,
            "render": function ( data, type, row ) {
                    return (data == 'Approved' ? '<a class="btn btn-sm" href="{{url('/download-sk-proposal')}}/'+row['id']+'"><i class="fa fa-download"></i> Download</a>' : '');
                }
        }
        ,{
            "targets": 8,
            "searchable": false,
            "render": function ( data, type, row ) {
                    return row['BatchType'].trim()+' '+row['TahunAjaran'].trim();
                }
        }
        ,{
            "targets": 13,
            "searchable": false,
            "render": function ( data, type, row ) {
                    return '<a href="{{url('/edit-add-proposal/')}}/'+data+'" class="editor_edit">Edit</a>';
                }
        } ]
    });



});
</script>
@endsection
