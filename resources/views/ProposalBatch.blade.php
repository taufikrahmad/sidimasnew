@extends('main')

@section('title','| Configurations ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Laporan Proposal
        <small>Laporan Proposal</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Laporan Proposal</li>
      </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Laporan Proposal</h3>
          <div class="box-tools pull-right">
            &nbsp;
          </div>
        </div>
        <div class="box-body">
            <form action="{{url('/download/report-proposal-batch')}}" id="formConfiguration" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="batchType" class="col-sm-2 control-label">Batch</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="hidden" name="idBatch" id="idBatch" readonly value="">
                                <select id="batchType" name="FIDBatch" class="form-control">
                                    <option value=""> -- Select Batch Type -- </option>
                                    @foreach ($batch as $data)
                                        <option value="{{$data->id}}">{{$data->BatchType}} {{$data->TahunAjaran}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                                <div class="col-md-4 col-md-offset-2 col-sm-12">
                                    <button type="submit" class="btn btn-success">Download</button>
                                </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

$(function() {
    
});
</script>
@endsection
