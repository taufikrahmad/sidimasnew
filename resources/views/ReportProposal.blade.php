@extends('main')

@section('title','| Configurations ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Batch
        <small>Edit Batch</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Batch Management</a></li>
        <li class="active">Edit Batch</li>
      </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Create Batch</h3>
          <div class="box-tools pull-right">
            &nbsp;
          </div>
        </div>
        <div class="box-body">
            <form action="{{url('/edit-batch-management')}}/{{$model->id}}" id="formConfiguration" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="batchType" class="col-sm-2 control-label">Batch</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="hidden" name="idBatch" id="idBatch" readonly value="{{$model->id}}">
                                <select id="batchType" name="batchType" class="form-control">
                                    <option value=""> -- Select Batch Type -- </option>
                                    <option {{(trim($model->BatchType) == "Internal" ? "selected" : "")}} value="Internal"> [Internal] </option>
                                    <option {{(trim($model->BatchType) == "Eksternal" ? "selected" : "")}} value="Eksternal"> [Eksternal] </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tahunAjaran" class="col-sm-2 control-label">Tahun Ajaran</label>
                            <div class="col-md-4 col-sm-12">
                                <select id="tahunAjaran" name="tahunAjaran" class="form-control">
                                        <option value=""> -- Select Tahun Ajaran -- </option>
                                        <option {{(trim($model->TahunAjaran) == "2016/2017" ? "selected" : "")}} value="2016/2017"> [2016/2017] </option>
                                        <option {{(trim($model->TahunAjaran) == "2017/2018" ? "selected" : "")}} value="2017/2018"> [2017/2018] </option>
                                        <option {{(trim($model->TahunAjaran) == "2018/2019" ? "selected" : "")}} value="2018/2019"> [2018/2019] </option>
                                        <option {{(trim($model->TahunAjaran) == "2019/2020" ? "selected" : "")}} value="2019/2020"> [2019/2020] </option>
                                        <option {{(trim($model->TahunAjaran) == "2020/2021" ? "selected" : "")}} value="2020/2021"> [2020/2021] </option>
                                        <option {{(trim($model->TahunAjaran) == "2021/2022" ? "selected" : "")}} value="2021/2022"> [2021/2022] </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="batchuploadDate" class="col-sm-2 control-label">Last Upload</label>
                            <div class="col-md-4 col-sm-12">
                                <input type="text" value="{{date('m/d/Y', strtotime($model->batch_upload_proposal))}}" name="batchuploadDate" id="batchuploadDate" class="form-control datepicker" />
                            </div>
                        </div>
                        <div class="form-group">
                                <label for="batchuploadDate" class="col-sm-2 control-label">Review Proposal</label>
                                <div class="col-md-4 col-sm-12">
                                    <input type="text" value="{{date('m/d/Y', strtotime($model->review_proposal))}}" name="review_proposal" id="review_proposal" class="form-control datepicker" />
                                </div>
                        </div>
                        <div class="form-group">
                                <label for="batchuploadDate" class="col-sm-2 control-label">Pengumuman Hasil Review Proposal</label>
                                <div class="col-md-4 col-sm-12">
                                    <input type="text" value="{{date('m/d/Y', strtotime($model->pengumuman_hasil_review_proposal))}}" name="pengumuman_hasil_review_proposal" id="pengumuman_hasil_review_proposal" class="form-control datepicker" />
                                </div>
                        </div>
                        <div class="form-group">
                                <label for="batchuploadDate" class="col-sm-2 control-label">Batas Upload Perbaikan</label>
                                <div class="col-md-4 col-sm-12">
                                    <input type="text" value="{{date('m/d/Y', strtotime($model->batas_upload_perbaikan))}}" name="batas_upload_perbaikan" id="batas_upload_perbaikan" class="form-control datepicker" />
                                </div>
                        </div>
                        <div class="form-group">
                                <label for="batchuploadDate" class="col-sm-2 control-label">Batas Laporan Hasil</label>
                                <div class="col-md-4 col-sm-12">
                                    <input type="text" value="{{date('m/d/Y', strtotime($model->batas_laporan_hasil))}}" name="batas_laporan_hasil" id="batas_laporan_hasil" class="form-control datepicker" />
                                </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2 col-sm-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

$(function() {
    
});
</script>
@endsection
