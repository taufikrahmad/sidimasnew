@extends('main')

@section('title','| Master User-Role ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        User Role
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li class="active">User Role</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> User Role</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered" id="table-user-role">
                        <thead>
                            <tr>
                                <th>NIDN</th>
                                <th>NIP</th>
                                <th>Name</th>
                                <th>Faculty</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">

$(function() {

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $('#table-user-role').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/datatables-user-roles',
        columns: [
            
            { data: 'NIDN', name: 'NIDN' },
            { data: 'NIP', name: 'NIP' },
            { data: 'FirstName', name: 'FirstName' },
            { data: 'FacultyDesc', name: 'FacultyDesc' },
            {
                data: 'idUser',
                className: "center"
            }
        ],
        "columnDefs": [ 
            {
            "targets": [0,1],
            "searchable": false,
            "render": function ( data, type, row ) {
                    return data.trim();
                }
            },
            {
            "targets": 2,
            "searchable": false,
            "render": function ( data, type, row ) {
                    return row['FirstName'].trim()+' '+row['MiddleName'].trim()+' '+row['LastName'].trim();
                }
            },
            {
            "targets": 4,
            "searchable": false,
            "render": function ( data, type, row ) {
                    return '<a href="{{url('/edit-user-role/')}}/'+data+'" class="editor_edit">Edit</a>';
                }
            }
        ]
    });



});
</script>
@endsection
