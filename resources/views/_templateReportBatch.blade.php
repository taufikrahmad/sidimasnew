<!DOCTYPE html >
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta charset="utf-8" />
</head>

<body style="margin: 0;">
	<table align="left" border="1" cellpadding="1" cellspacing="1" style="width:100%">
		<thead>
			<tr>
				<th scope="col">Batch Type</th>
				<th scope="col">Batch Upload Proposal</th>
				<th scope="col">Review Proposal</th>
				<th scope="col">Pengumuman Hasil Review Proposal</th>
				<th scope="col">Batas Upload Perbaikan</th>
				<th scope="col">Batas Laporan Hasil</th>
			</tr>
		</thead>
		<tbody>
			@foreach($batch as $data)
				<tr>
					<td>{{trim($data->BatchType)}}</td>
					<td>{{trim($data->batch_upload_proposal)}}</td>
					<td>{{trim($data->review_proposal)}}</td>
					<td>{{trim($data->pengumuman_hasil_review_proposal)}}</td>
					<td>{{trim($data->batas_upload_perbaikan)}}</td>
					<td>{{trim($data->batas_laporan_hasil)}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>

</html>