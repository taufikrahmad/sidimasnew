<!DOCTYPE html >
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta charset="utf-8" />
</head>

<body style="margin: 0;">
	<table align="left" border="1" cellpadding="1" cellspacing="1" style="width:100%">
		<thead>
			<tr>
				<th scope="col">Judul</th>
				<th scope="col">Provinsi Lokasi</th>
				<th scope="col">Kabupaten Lokasi</th>
				<th scope="col">Kecamatan Lokasi</th>
				<th scope="col">Jarak Lokasi</th>
				<th scope="col">Durasi/Satuan Durasi</th>
				<th scope="col">Budget</th>
				<th scope="col">Bank Name</th>
				<th scope="col">Account Number</th>
				<th scope="col">Transfer 1</th>
				<th scope="col">Transfer 2</th>
			</tr>
		</thead>
		<tbody>
			@foreach($payment as $data)
				<tr>
					<td>{{trim($data->Judul)}}</td>
					<td>{{trim($data->Provinsi)}}</td>
					<td>{{trim($data->Kabupaten)}}</td>
					<td>{{trim($data->Kecamatan)}}</td>
					<td>{{trim($data->JarakLokasi)}}</td>
					<td>{{trim($data->Durasi)}} {{trim($data->SatuanDurasi)}}</td>
					<td>Rp. {{number_format(($data->budget1+$data->budget2),2)}}</td>
					<td>{{trim($data->BankName)}}</td>
					<td>{{trim($data->AccountNumber)}}</td>
					<td>Rp. {{number_format(($data->TransferAmount),2)}}</td>
					<td>Rp. {{number_format(($data->TransferAmount2),2)}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>

</html>