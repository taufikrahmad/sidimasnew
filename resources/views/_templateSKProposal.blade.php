<?php 
	$alphas = range('a', 'z');
?>
<!DOCTYPE html >
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta charset="utf-8" />
	<style>
		@page {
			size: 21cm 29.7cm;
		}
	</style>
</head>

<body style="margin: 0;">
<div style="text-align:center"><span style="font-size:14px"><strong>Surat Pengesahan</strong></span><br />
			&nbsp;</div>
			
			<div style="text-align:justify">
			<table border="0" cellpadding="1" cellspacing="1" style="width:715px">
				<tbody>
					<tr>
						<td style="width:160px"><span style="font-size:14px"><strong>Judul</strong></span></td>
						<td style="width:1px"><span style="font-size:14px"><strong>:</strong></span></td>
						<td colspan="2" style="width:450px">{{trim($dataProposal[0]->Judul)}}</td>
					</tr>
					<tr>
						<td colspan="4" style="width:707px"><span style="font-size:14px"><strong>Ketua Penelitian</strong></span></td>
					</tr>
					<tr>
						<td style="width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">a. Nama Lengkap</span></div>
						</td>
						<td style="width:4px">
						<div><span style="font-size:14px">:</span></div>
						</td>
						<td colspan="2" style="width:450px">
						<div>{{trim($dataProposal[0]->FirstName).' '.trim($dataProposal[0]->MiddleName).' '.trim($dataProposal[0]->LastName)}}</div>
						</td>
					</tr>
					<tr>
						<td style="width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">b.&nbsp;NIDN</span></div>
						</td>
						<td style="width:4px">
						<div><span style="font-size:14px">:</span></div>
						</td>
						<td colspan="2" style="width:450px">
						<div>{{trim($dataProposal[0]->NIDN)}}</div>
						</td>
					</tr>
					<tr>
						<td style="width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">c. Jabatan Fungsional</span></div>
						</td>
						<td style="width:4px">
						<div><span style="font-size:14px">:</span></div>
						</td>
						<td colspan="2" style="width:450px">
						<div>{{trim($dataProposal[0]->NamaJfa)}}</div>
						</td>
					</tr>
					<tr>
						<td style="width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">d. Bidang Ilmu</span></div>
						</td>
						<td style="width:4px">
						<div><span style="font-size:14px">:</span></div>
						</td>
						<td colspan="2" style="width:450px">
						<div>{{trim($dataProposal[0]->NamaFakultas)}}</div>
						</td>
					</tr>
					<tr>
						<td style="width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">e. Nomor HP</span></div>
						</td>
						<td style="width:4px">
						<div><span style="font-size:14px">:</span></div>
						</td>
						<td colspan="2" style="width:450px">
						<div>{{trim($dataProposal[0]->Phone)}}</div>
						</td>
					</tr>
					<tr>
						<td style="width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">f. Alamat Surel (email)</span></div>
						</td>
						<td style="width:4px"><span style="font-size:14px">:</span></td>
						<td colspan="2" style="width:450px">{{trim($dataProposal[0]->Email)}}</td>
					</tr>
					<tr>
						<td colspan="4" style="width:707px"><span style="font-size:14px"><strong>Anggota Tim Pengusul</strong></span></td>
					</tr>
					<tr>
						<td style="width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">a. Jumlah Anggota</span></div>
						</td>
						<td style="width:4px"><span style="font-size:14px">:</span></td>
						<td colspan="2" style="width:450px"><span style="font-size:14px">Dosen {{count($dataAnggotaDosen)}} Orang</span></td>
					</tr>
					@foreach($dataAnggotaDosen as $data)
					<tr>
						<td style="text-align:left; width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">{{$alphas[$loop->iteration]}}. Nama Anggota {{$loop->iteration}}</span></div>
						</td>
						<td style="width:4px"><span style="font-size:14px">:</span></td>
						<td colspan="2" style="width:450px">
								{{trim($data->FirstName).' '.trim($data->MiddleName).' '.trim($data->LastName).' - '.trim($data->NIDN)}}
						</td>
					</tr>
					@endforeach
					<tr>
						<td style="width:160px">&nbsp;</td>
						<td style="width:4px">&nbsp;</td>
						<td colspan="2" style="width:450px">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="4" style="width:707px"><span style="font-size:14px"><strong>Mahasiswa</strong></span></td>
					</tr>
					<tr>
						<td style="width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">a. Jumlah Mahasiswa</span></div>
						</td>
						<td style="width:4px"><span style="font-size:14px">:</span></td>
						<td colspan="2" style="width:450px"><span style="font-size:14px">Mahasiswa {{count($dataAnggotaDosen)}} Orang</span></td>
					</tr>
					@foreach($dataAnggotaMahasiswa as $data)
					<tr>
						<td style="text-align:left; width:160px">
						<div style="margin-left:40px"><span style="font-size:14px">{{$alphas[$loop->iteration]}}. Mahasiswa {{$loop->iteration}}</span></div>
						</td>
						<td style="width:4px"><span style="font-size:14px">:</span></td>
						<td colspan="2" style="width:450px">{{trim($data->Name).' - '.trim($data->NIM)}}</td>
					</tr>
					@endforeach
					<tr>
						<td style="width:160px">&nbsp;</td>
						<td style="width:4px">&nbsp;</td>
						<td colspan="2" style="width:450px">&nbsp;</td>
					</tr>
					<tr>
						<td style="width:160px"><span style="font-size:14px"><strong>Lama Riset Keseluruhan</strong></span></td>
						<td style="width:4px"><span style="font-size:14px"><strong>:</strong></span></td>
						<td colspan="2" style="width:450px">{{$dataProposal[0]->Durasi}} 
							{{trim($dataProposal[0]->SatuanDurasi)}}</td>
					</tr>
					<tr>
						<td style="width:160px"><span style="font-size:14px"><strong>Lokasi</strong></span></td>
						<td style="width:4px"><span style="font-size:14px"><strong>:</strong></span></td>
						<td colspan="2" style="width:450px">
							Kec. {{trim($dataProposal[0]->KecamatanLokasi)}} <br />
							Kab. {{trim($dataProposal[0]->KabupatenLokasi)}} <br />
							Prov. {{trim($dataProposal[0]->ProvinsiLokasi)}} <br />
						</td>
					</tr>
					<tr>
						<td style="width:160px"><span style="font-size:14px"><strong>Luaran Penelitian</strong></span></td>
						<td style="width:4px"><span style="font-size:14px"><strong>:</strong></span></td>
						<td colspan="2" style="width:450px">
							{{trim($dataProposal[0]->RencanaLuaran)}}
						</td>
					</tr>
					<tr>
						<td style="width:160px"><span style="font-size:14px"><strong>Biaya Riset Keseluruhan</strong></span></td>
						<td style="width:4px"><span style="font-size:14px"><strong>:</strong></span></td>
						<td colspan="2" style="width:450px">
							Rp. {{number_format(($dataProposal[0]->budget1+$dataProposal[0]->budget2),2)}}
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:right; width:690px"><span style="font-size:14px">
								Jakarta, 03 Desember 2019&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						</span></td>
					</tr>
					<tr>
						<td colspan="4" style="width:707px"><span style="font-size:14px">Mengetahui</span></td>
					</tr>
					<tr>
						<td style="width:160px">&nbsp;</td>
						<td style="width:4px">&nbsp;</td>
						<td style="width:218px">&nbsp;</td>
						<td style="width:182px">&nbsp;</td>
					</tr>
					<tr>
						<td style="width:160px">&nbsp;</td>
						<td style="width:4px">&nbsp;</td>
						<td style="width:218px">&nbsp;</td>
						<td style="width:182px">&nbsp;</td>
					</tr>
					<tr>
						<td style="width:160px">&nbsp;</td>
						<td style="width:4px">&nbsp;</td>
						<td style="width:218px">&nbsp;</td>
						<td style="width:182px">&nbsp;</td>
					</tr>
					<tr>
						<td style="width:160px">&nbsp;</td>
						<td style="width:4px">&nbsp;</td>
						<td style="width:218px">&nbsp;</td>
						<td style="width:182px">&nbsp;</td>
					</tr>
					
				</tbody>
			</table>
			</div>
			
</body>

</html>