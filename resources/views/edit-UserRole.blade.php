@extends('main')

@section('title','| Master User-Role ')
@section('style')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        User Role
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li>User Role</li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="col-sm-12">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> User Role</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="{{url('/edit-user-role/')}}/{{$dataDosen[0]->idUser}}" id="formConfiguration" enctype="multipart/form-data" class="form-horizontal" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="batchType" class="col-sm-2 control-label">NIDN</label>
                                    <div class="col-md-4 col-sm-12">
                                        <input type="text" readonly name="NIDN" id="NIDN" class="form-control" value="{{trim($dataDosen[0]->NIDN)}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="batchType" class="col-sm-2 control-label">NIP</label>
                                    <div class="col-md-4 col-sm-12">
                                        <input type="text" readonly name="NIP" id="NIP" class="form-control" value="{{trim($dataDosen[0]->NIP)}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="batchType" class="col-sm-2 control-label">Name</label>
                                    <div class="col-md-4 col-sm-12">
                                        <input type="text" readonly name="Name" id="Name" class="form-control" value="{{trim($dataDosen[0]->FirstName).' '.trim($dataDosen[0]->MiddleName).' '.trim($dataDosen[0]->LastName)}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="role" class="col-sm-2 control-label">Role</label>
                                    <div class="col-md-4 col-sm-12">
                                        <select name="role" id="role" class="form-control">
                                            <option value=""> -- Choose Role -- </option>
                                            @foreach ($dataMRoles as $role)
                                                <option value="{{$role->id}}">{{trim($role->Description)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                    
                                <div class="form-group">
                                    <div class="col-md-4 col-md-offset-2 col-sm-12">
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                        <table class="table table-bordered" id="table-user-role">
                            <thead>
                                <tr>
                                    <th>Role</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- END Main content -->
@endsection

@section('pagejs')
<script type="text/javascript">
$(function() {

$.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#table-user-role').DataTable({
    processing: true,
    serverSide: true,
    ajax: '/datatables-user-roles/{{$dataDosen[0]->idUser}}',
    columns: [
        
        { data: 'RoleDesc', name: 'RoleDesc' },
        {
            data: 'idUserRole',
            className: "center"
        }
    ],
    "columnDefs": [ 
        {
        "targets": [0],
        "searchable": false,
        "render": function ( data, type, row ) {
                return data.trim();
            }
        },
        {
        "targets": 1,
        "searchable": false,
        "render": function ( data, type, row ) {
                return '<a href="{{url('/delete-user-role/')}}/'+data+'" class="editor_edit">Delete</a>';
            }
        }
    ]
});



});
</script>
@endsection
