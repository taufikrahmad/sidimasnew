<!doctype html>
<html @lang('en')>
    <head>
        @include('partials/_head')
    </head>
<body class="hold-transition skin-blue sidebar-mini">
    @include('partials/_nav')
    @include('partials/_aside')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
              @endif
          
              @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $message }}</strong>
                </div>
              @endif
          
              @if ($message = Session::get('warning'))
                <div class="alert alert-warning alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $message }}</strong>
              </div>
              @endif
          
              @if ($message = Session::get('info'))
                <div class="alert alert-info alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $message }}</strong>
                </div>
              @endif
          
              @if ($errors->any())
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  Please check the form below for errors
              </div>
              @endif
            @yield('content')
        </div>
        @include('partials/_footer')
    </div>

    
    @include('partials/_script')
    @yield('pagejs')
</body>
</html>
