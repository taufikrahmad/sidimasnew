
      <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Batch Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/batch-management') }}"><i class="fa fa-circle-o"></i> Batch</a></li>
          </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-address-card"></i> <span>Reviewer Assignment</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/assignment') }}"><i class="fa fa-circle-o"></i> Assignment</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
        <i class="fa fa-book"></i> <span>Proposal</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/proposal') }}"><i class="fa fa-circle-o"></i> Upload</a></li>
          <li><a href="{{ url('/review') }}"><i class="fa fa-circle-o"></i> Review</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
        <i class="fa fa fa-dollar"></i> <span>Payment Monitoring</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/payment-records') }}"><i class="fa fa-circle-o"></i> Payment Records</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
        <i class="fa fa fa-file"></i> <span>Report</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="/download/report-batch"><i class="fa fa-circle-o"></i> Batch </a></li>
          <li><a href="/report-proposal-batch"><i class="fa fa-circle-o"></i> Proposal</a></li>
          <li><a href="/report-payment-batch"><i class="fa fa-circle-o"></i> Payment</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
        <i class="fa fa-gear"></i> <span>Configurations</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('Configurations') }}"><i class="fa fa-circle-o"></i> Configurations</a></li>
          <li><a href="{{ url('master-target') }}"><i class="fa fa-circle-o"></i> Target Capaian</a></li>
          <li><a href="{{ url('master-jfa') }}"><i class="fa fa-circle-o"></i> Jabatan Fungsional</a></li>
          <li><a href="{{ url('master-job-roles') }}"><i class="fa fa-circle-o"></i> Role</a></li>
          <li><a href="{{ url('master-user-roles') }}"><i class="fa fa-circle-o"></i> User Role</a></li>
        </ul>
      </li>