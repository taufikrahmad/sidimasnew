<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/
//auth
Route::get('/','AuthController@index');
Route::post('/login-post','AuthController@loginPost');
Route::get('/register','AuthController@register');
Route::post('/register-post','AuthController@registerPost');
Route::get('/logout','AuthController@logout');

//batch
Route::get('/batch-management','BatchManagementController@index');
Route::get('/datatables-batch','BatchManagementController@datatablesTbltBatch');
Route::get('/add-batch-management','BatchManagementController@addBatchManagement');
Route::post('/add-batch-management','BatchManagementController@postAddBatchManagement');
Route::get('/edit-batch-management/{id}','BatchManagementController@editBatchManagement');
Route::post('/edit-batch-management/{id}','BatchManagementController@postEditBatchManagement');
Route::get('/delete-batch-management/{id}','BatchManagementController@deleteBatchManagement');

//proposal
Route::get('/proposal','ProposalController@index');
Route::get('/datatables-proposal','ProposalController@datatablesProposal');
Route::get('/add-proposal','ProposalController@addProposal');
Route::post('/add-proposal','ProposalController@postAddProposal');
Route::get('/edit-add-proposal/{id}','ProposalController@editProposal');
Route::post('/edit-proposal/{id}','ProposalController@postEditProposal');
Route::get('/download-doc-proposal/{id}','ProposalController@downloadDocProposal');
Route::get('/download-sk-proposal/{id}','ProposalController@downloadSKProposal');
Route::get('/anggota-proposal/Mahasiswa/{id}','ProposalController@AnggotaMahasiswa');
Route::get('/anggota-proposal/Dosen/{id}','ProposalController@AnggotaDosen');
Route::get('/download/lembar-persetujuan/{id}','ProposalController@cetakPersetujuan');


//assigment
Route::get('/assignment','AssignmentController@index');
Route::get('/datatables-assignment','AssignmentController@datatablesAssignment');
Route::get('/edit-assignment/{id}','AssignmentController@editAssignment');
Route::post('/edit-assignment/{id}','AssignmentController@postEditAssignment');



//tblm_target
Route::get('/master-target','TbltTargetProposalController@index');
Route::get('/datatables-mtarget','TbltTargetProposalController@datatablesMTarget');
Route::get('/add-master-target','TbltTargetProposalController@addTarget');
Route::post('/add-master-target','TbltTargetProposalController@postAddTarget');
Route::get('/edit-master-target/{id}','TbltTargetProposalController@editTarget');
Route::post('/edit-master-target/{id}','TbltTargetProposalController@postEditTarget');
Route::get('/delete-master-target/{id}','TbltTargetProposalController@deleteTarget');

//jfa
Route::get('/master-jfa','TblmJfaController@index');
Route::get('/datatables-jfa','TblmJfaController@datatablesJfa');
Route::get('/add-jfa','TblmJfaController@addJfa');
Route::post('/add-jfa','TblmJfaController@postAddJfa');
Route::get('/edit-jfa/{id}','TblmJfaController@editJfa');
Route::post('/edit-jfa/{id}','TblmJfaController@postEditJfa');
Route::get('/delete-jfa/{id}','TblmJfaController@deleteJfa');


//tblm_job_roles
Route::get('/master-job-roles','JobRoleController@index');
Route::get('/datatables-job-roles','JobRoleController@datatablesJobRoles');
Route::get('/add-job-roles','JobRoleController@addJobRoles');
Route::post('/add-job-roles','JobRoleController@postAddJobRoles');
Route::get('/edit-job-roles/{id}','JobRoleController@editJobRoles');
Route::post('/edit-job-roles/{id}','JobRoleController@postEditJobRoles');
Route::get('/delete-job-roles/{id}','JobRoleController@deleteJobRoles');

//tblm_mahasiswa
Route::get('/mahasiswa/find','TblmMahasiswaController@find');

//tblm_dosen
Route::get('/dosen/find','TblmDosenController@find');


//payment 
Route::get('/payment-records','PaymentRecordsController@index');
Route::get('/datatables-payment-records','PaymentRecordsController@datatablesPaymentRecords');
Route::get('/edit-payment-records/{id}','PaymentRecordsController@editPaymentRecords');
Route::post('/edit-payment-records/{id}','PaymentRecordsController@postEditPaymentRecords');

//review
Route::get('/review','ReviewController@index');
Route::get('/datatables-review','ReviewController@datatablesAssignment');
Route::get('/edit-review/{id}','ReviewController@edit');
Route::post('/edit-review/{id}','ReviewController@update');



//tblm_user_role
Route::get('/master-user-roles','UserRoleController@index');
Route::get('/datatables-user-roles','UserRoleController@datatablesDosen');
Route::get('/datatables-user-roles/{id}','UserRoleController@datatablesDosenRole');
Route::get('/edit-user-role/{id}','UserRoleController@edit');
Route::post('/edit-user-role/{id}','UserRoleController@postedit');
Route::get('/delete-user-role/{id}','UserRoleController@delete');


//Report
Route::get('/download/report-batch','ReportController@cetakReportBatch');
Route::get('/report-proposal-batch','ReportController@ReportProposalBatch');
Route::post('/download/report-proposal-batch','ReportController@cetakReportProposalBatch');
Route::get('/report-payment-batch','ReportController@ReportPaymentBatch');
Route::post('/download/report-payment-batch','ReportController@cetakReportPaymentBatch');


Route::get('/quiz','QuizController@index');
Route::get('/quiz/post','QuizController@form');
Route::post('/quiz/post','QuizController@save');